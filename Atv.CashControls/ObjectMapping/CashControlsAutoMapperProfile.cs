﻿using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Dtos.Books;
using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Depts;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Dtos.Incomes;
using Atv.CashControls.Dtos.Loans;
using Atv.CashControls.Dtos.Wallets;
using Atv.CashControls.Entities;
using Atv.CashControls.Pages.BankingNotes;
using Atv.CashControls.Pages.CashCategories;
using Atv.CashControls.Pages.Depts;
using Atv.CashControls.Pages.Expenses;
using Atv.CashControls.Pages.Loans;
using Atv.CashControls.Pages.Wallets;
using AutoMapper;
using Volo.Abp.AutoMapper;

namespace Atv.CashControls.ObjectMapping;

public class CashControlsAutoMapperProfile : Profile
{
    public CashControlsAutoMapperProfile()
    {
        CreateMap<string, Guid>();
        CreateMap<Guid, string>();

        /* Create your AutoMapper object mappings here */
        CreateMap<CashCategory, CashCategoryDto>();
        CreateMap<CashCategoryDto, CashCategory>();
        CreateMap<CreateCashCategoryDto, CashCategory>()
            .Ignore(x => x.Id);
        CreateMap<CreateCashCategoryViewModel, CreateCashCategoryDto>();
        CreateMap<UpdateCashCategoryDto, CashCategory>()
            .Ignore(x => x.Id);
        CreateMap<UpdateCashCategoryViewModel, CashCategoryDto>();
        CreateMap<CashCategoryDto, UpdateCashCategoryViewModel>();

        CreateMap<Expense, ExpenseDto>()
            .ForMember(x => x.Month, e => e.MapFrom(x => x.Date.ToString("MM/yyyy")))
            .ForMember(x => x.CategoryName, e => e.MapFrom(x => x.CashCategory.Name));
        CreateMap<ExpenseDto, Expense>();
        CreateMap<ExpenseDto, EditExpenseViewModel>();
        CreateMap<EditExpenseViewModel, UpdateExpenseDto>();
        CreateMap<UpdateExpenseDto, Expense>()
            .ForMember(x => x.Price, e => e.MapFrom(x => decimal.Parse(x.Price)))
            .Ignore(x => x.Id);
        CreateMap<CreateExpenseDto, Expense>()
            .ForMember(x => x.Price, e => e.MapFrom(x => decimal.Parse(x.Price)))
            .Ignore(x => x.Id);

        CreateMap<Dept, DeptsDto>();
        CreateMap<DeptsDto, Dept>();
        CreateMap<Dept, UpdateDeptViewModel>();
        CreateMap<CreateDeptsDto, Dept>()
            .Ignore(x => x.Id);
        CreateMap<UpdateDeptsDto, Dept>()
            .Ignore(x => x.Id);

        CreateMap<Loan, LoansDto>();
        CreateMap<LoansDto, Loan>();
        CreateMap<CreateLoansDto, Loan>()
            .ForMember(x => x.Amount, e => e.MapFrom(x => decimal.Parse(x.Amount)))
            .Ignore(x => x.Id);
        CreateMap<UpdateLoansDto, Loan>()
            .ForMember(x => x.Amount, e => e.MapFrom(x => decimal.Parse(x.Amount)))
            .Ignore(x => x.Id);
        CreateMap<CreateLoanViewModel, CreateLoansDto>();


        // Banking Note 
        CreateMap<BankingNote, BankingNoteDto>();
        CreateMap<CreateBankingNoteDto, BankingNote>()
            .ForMember(x => x.Money, e => e.MapFrom(x => decimal.Parse(x.Money)))
            .Ignore(x => x.Id);
        CreateMap<UpdateBankingNoteDto, BankingNote>()
            .ForMember(x => x.Money, e => e.MapFrom(x => decimal.Parse(x.Money)))
            .Ignore(x => x.Id);
        CreateMap<CreateBankingNoteViewModel, CreateBankingNoteDto>();
        CreateMap<BankingNoteDto, UpdateBankingNoteViewModel>();
        CreateMap<UpdateBankingNoteViewModel, UpdateBankingNoteDto>();

        // Banking note transaction
        CreateMap<BankingNoteTransaction, BankingNoteTransactionDto>();
        CreateMap<CreateBankingNoteTransactionDto, BankingNoteTransaction>()
            .ForMember(x => x.Amount, e => e.MapFrom(x => decimal.Parse(x.Amount)))
            .Ignore(x => x.Id);
        CreateMap<CreateBankingTransactionViewModel, CreateBankingNoteTransactionDto>();
        CreateMap<UpdateBankingNoteTransactionDto, BankingNoteTransaction>()
            .ForMember(x => x.Amount, e => e.MapFrom(x => decimal.Parse(x.Amount)))
            .Ignore(x => x.Id);
        CreateMap<UpdateBankingTransactionViewModel, UpdateBankingNoteTransactionDto>();
        CreateMap<BankingNoteTransactionDto, UpdateBankingTransactionViewModel>();

        // Book
        CreateMap<Book, BookDto>().ReverseMap();

        // Wallets
        CreateMap<Wallet, WalletDto>();
        CreateMap<CreateWalletDto, Wallet>()
            .ForMember(x => x.Money, e => e.MapFrom(x => decimal.Parse(x.Money)))
            .Ignore(x => x.Id);
        CreateMap<UpdateWalletDto, Wallet>()
            .ForMember(x => x.Money, e => e.MapFrom(x => decimal.Parse(x.Money)))
            .Ignore(x => x.Id);
        CreateMap<CreateWalletViewModel, CreateWalletDto>();
        CreateMap<WalletDto, UpdateWalletViewModel>();
        CreateMap<UpdateWalletViewModel, UpdateWalletDto>();

        CreateMap<Income, IncomeDto>();
        CreateMap<CreateIncomeDto, Income>()
            .Ignore(x => x.Id);
        CreateMap<UpdateIncomeDto, Income>()
            .Ignore(x => x.Id);
    }
}
