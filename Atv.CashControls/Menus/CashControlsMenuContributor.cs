﻿using Atv.CashControls.Localization;
using Atv.CashControls.Permissions;
using Volo.Abp.Identity.Web.Navigation;
using Volo.Abp.SettingManagement.Web.Navigation;
using Volo.Abp.TenantManagement.Web.Navigation;
using Volo.Abp.UI.Navigation;

namespace Atv.CashControls.Menus;

public class CashControlsMenuContributor : IMenuContributor
{
	public async Task ConfigureMenuAsync(MenuConfigurationContext context)
	{
		if (context.Menu.Name == StandardMenus.Main)
		{
			await ConfigureMainMenuAsync(context);
		}
	}

	private Task ConfigureMainMenuAsync(MenuConfigurationContext context)
	{
		var administration = context.Menu.GetAdministration();
		var l = context.GetLocalizer<CashControlsResource>();

		context.Menu.Items.Insert(
			0,
			new ApplicationMenuItem(
				CashControlsMenus.Home,
				l["Menu:Home"],
				"~/",
				icon: "fas fa-home",
				order: 0
			)
		);

		context.Menu.Items.Insert(
			1,
			new ApplicationMenuItem(
				CashControlsMenus.Dashboard,
				l["Menu:Dashboard"],
				"~/Dashboard",
				icon: "fas fa-gauge",
				order: 1
			)
		);

		context.Menu.Items.Insert(
			1,
			new ApplicationMenuItem(
				CashControlsMenus.Dashboard,
				l["Menu:Wallets"],
				"~/Wallets",
				icon: "fas fa-wallet",
				order: 2,
				requiredPermissionName: CashControlsPermissions.Wallets.Default
			)
		);

		context.Menu.Items.Insert(
			2,
			new ApplicationMenuItem(
				CashControlsMenus.CashCategories,
				l["Menu:CashCategories"],
				"~/CashCategories",
				icon: "fas fa-list",
				order: 2,
				requiredPermissionName: CashControlsPermissions.CashCategories.Default
			)
		);

		context.Menu.Items.Insert(
			3,
			new ApplicationMenuItem(
				CashControlsMenus.Expenses,
				l["Menu:Expenses"],
				"~/Expenses",
				icon: "fas fa-cash-register",
				order: 3,
				requiredPermissionName: CashControlsPermissions.Expenses.Default
			)
		);

        context.Menu.Items.Insert(
            3,
            new ApplicationMenuItem(
                CashControlsMenus.Incomes,
                l["Menu:Incomes"],
                "~/Incomes",
                icon: "fas fa-cash-register",
                order: 3,
                requiredPermissionName: CashControlsPermissions.Incomes.Default
            )
        );

        context.Menu.Items.Insert(
			4,
			new ApplicationMenuItem(
				CashControlsMenus.Depts,
				l["Menu:Depts"],
				"~/Depts",
				icon: "fa fa-money-bill-transfer",
				order: 4,
				requiredPermissionName: CashControlsPermissions.Depts.Default
			)
		 );

        context.Menu.Items.Insert(
            5,
            new ApplicationMenuItem(
                CashControlsMenus.Loans,
                l["Menu:Loans"],
                "~/Loans",
                icon: "fa fa-landmark",
                order: 5,
                requiredPermissionName: CashControlsPermissions.Loans.Default
            )
         );

        context.Menu.Items.Insert(5, new ApplicationMenuItem(
			CashControlsMenus.BankingNotes,
			l["Menu:BankingNotes"],
			"~/BankingNotes",
			icon: "fa fa-building-columns",
			order: 6,
			requiredPermissionName: CashControlsPermissions.BankingNotes.Default
		));

		context.Menu.AddItem(new ApplicationMenuItem("DemoLearns", l["Menu:DemoLearns"], icon: "fa fa-laptop-code")
				.AddItem(new ApplicationMenuItem("BookStores", l["Menu:BookStore"], icon: "fa fa-book")
					.AddItem(new ApplicationMenuItem(CashControlsMenus.Books,l["Menu:Books"],url: "~/DemoLearns/Books"))
				)
				.AddItem(new ApplicationMenuItem("WidgetsDemo", l["Menu:WidgetsDemo"], icon: "fa fa-border-none")
					.AddItem(new ApplicationMenuItem(CashControlsMenus.Widgets, l["Menu:Widgets"], url: "~/DemoLearns/Widgets"))
				)
        );

		//context.Menu.GetAdministration().AddItem(administration);

		if (CashControlsModule.IsMultiTenant)
		{
			administration.SetSubItemOrder(TenantManagementMenuNames.GroupName, 1);
		}
		else
		{
			administration.TryRemoveMenuItem(TenantManagementMenuNames.GroupName);
		}

		return Task.CompletedTask;
	}
}
