﻿namespace Atv.CashControls.Menus;

public class CashControlsMenus
{
    private const string Prefix = "CashControls";
    public const string Home = Prefix + ".Home";
    public const string CashCategories = Prefix + "CashCategories";
    public const string Expenses = Prefix + "Expenses";
    public const string Dashboard = Prefix + "Dashboard";
    public const string Depts = Prefix + "Depts";
    public const string Loans = Prefix + "Loans";
    public const string BankingNotes = Prefix + "BankingNotes";
    public const string Wallets = Prefix + "Wallets";
    public const string Incomes = Prefix + "Incomes";

    public const string Books = Prefix + "Books";
    public const string Widgets = Prefix + "Widgets";

    //Add your menu items here...

}
