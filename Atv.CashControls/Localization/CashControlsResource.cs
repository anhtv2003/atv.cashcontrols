﻿using Volo.Abp.Localization;

namespace Atv.CashControls.Localization;

[LocalizationResourceName("CashControls")]
public class CashControlsResource
{
    
}