﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Identity;

namespace Atv.CashControls.Entities
{
    public class BankingNoteTransaction : AuditedAggregateRoot<Guid>
    {
        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        public TransactionTypeEnum TransactionType { get; set; }

        public string? Place { get; set; } // Nơi giao dịch

        public string? BankingReceive { get; set; }

        public string? Receiver { get; set; }

        public string? Remitter { get; set; }

        public string? Image { get; set; }

        public Guid BankingNoteId { get; set; }

        [ForeignKey(nameof(BankingNoteId))]
        public BankingNote bankingNote { get; set; }

    }
}
