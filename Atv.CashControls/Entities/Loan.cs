﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Identity;

namespace Atv.CashControls.Entities
{
    [Table("Loans")]
    public class Loan : AuditedAggregateRoot<Guid>
    {
        [Required]
        public decimal Amount { get; set; } // Số tiền cho vay

        public string Borrower { get; set; } // Người vay

        [Required]
        public DateTime Date { get; set; } // Ngày cho vay

        [Required]
        public DateTime? DueDate { get; set; } // Ngày người vay phải trả.

        public string? Description { get; set; }

        public DeptStatusEnum Status { get; set; }
    }
}
