﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Identity;

namespace Atv.CashControls.Entities
{
    public class CashCategory : AuditedAggregateRoot<Guid>
    {
        public string Name { get; set; }
        
        public string? Description { get; set; }

        public Status Status { get; set; }

        public ICollection<Expense> Expenses { get; set; }
    }
}
