﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;

namespace Atv.CashControls.Entities
{
    [Table("Incomes")]
    public class Income : AuditedAggregateRoot<Guid>
    {
        //public string Name { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public IncomeTypeEnum Type { get; set; } //lương
        public string? Source { get; set; } // công ty...
        public string? Description { get; set; }
    }
}
