﻿using Volo.Abp.Domain.Entities.Auditing;

namespace Atv.CashControls.Entities
{
    public class Wallet : AuditedAggregateRoot<Guid>
    {
        public string Name { get; set; }

        public decimal Money { get; set; }

        public string? Description { get; set; }
    }
}
