﻿using Volo.Abp.Domain.Entities.Auditing;

namespace Atv.CashControls.Entities
{
    public class BankingNote : AuditedAggregateRoot<Guid>
    {
        public string BankingName { get; set; }

        public string? BankingLogo { get; set; }

        public decimal Money { get; set; }

        public ICollection<BankingNoteTransaction> Transactions { get; set; }
    }
}
