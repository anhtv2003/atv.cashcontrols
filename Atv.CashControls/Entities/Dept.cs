﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Identity;

namespace Atv.CashControls.Entities
{
    [Table("Depts")]
    public class Dept : AuditedAggregateRoot<Guid>
    {
        [Required]
        public decimal Price { get; set; }

        public string? Lender { get; set; } // Người cho vay

        [Required]
        public DateTime Date { get; set; } // Ngày vay

        [Required]
        public DateTime DueDate { get; set; } // Ngày đến hạn

        public DateTime? ActualPaymentDate { get; set; } // Ngày trả thực tế

        public string? Description { get; set; }

        public DeptStatusEnum Status { get; set; }
    }
}
