﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.Identity;

namespace Atv.CashControls.Entities
{
    public class Expense : AuditedAggregateRoot<Guid>
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public DateTime Date { get; set; }

        public Guid? CategoryID { get; set; }

        [ForeignKey(nameof(CategoryID))]
        public CashCategory CashCategory { get; set; }

        public string? Note { get; set; }

        public Status Status { get; set; }

        public Guid? WalletId { get; set; }
    }
}
