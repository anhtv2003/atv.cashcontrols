﻿namespace Atv.CashControls.Enums
{
    public enum Unit
    {
        Bó = 0,
        Túi = 1,
        Bao = 2,
        Kg = 3,
        Lạng = 4
    }
}
