﻿namespace Atv.CashControls.Enums
{
    public enum TransactionTypeEnum
    {
        RutTien = 1,
        ChuyenTien = 2,
        NhanTien = 3,
        ChiTieu = 4,
    }
}
