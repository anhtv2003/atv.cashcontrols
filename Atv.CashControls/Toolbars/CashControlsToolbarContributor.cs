﻿using Atv.CashControls.Pages.Shared.Components.Notifications;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Toolbars;

namespace Atv.CashControls.Toolbars
{
    public class CashControlsToolbarContributor : IToolbarContributor
    {
        public Task ConfigureToolbarAsync(IToolbarConfigurationContext context)
        {
            if (context.Toolbar.Name == StandardToolbars.Main)
            {
                context.Toolbar.Items.Insert(
                    0, 
                    new ToolbarItem(typeof(NotificationViewComponent)));
            }

            return Task.CompletedTask;
        }
    }
}
