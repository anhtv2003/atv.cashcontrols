﻿using Asp.Versioning;
using Atv.CashControls.Dtos.Commons;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Services.Expenses;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Expenses")]
    [Route("/api/cash-controls/expenses")]
    public class ExpenseController : CashControlsController
    {
        private readonly IExpenseService _service;

        public ExpenseController(IExpenseService service)
        {
            _service = service;
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task DeleteAsync(Guid id)
        {
            return _service.DeleteAsync(id);
        }

        [HttpGet]
        [Route("get-by-id/{id}")]
        public async Task<ExpenseDto> GetAsync(Guid id)
        {
            return await _service.GetAsync(id);
        }

        [HttpGet]
        [Route("get-list")]
        public async Task<PagedResultDto<ExpenseDto>> GetListAync(GetExpensesInput input)
        {
            return await _service.GetListAync(input);
        }


        [HttpGet]
        [Route("expense-count")]
        public async Task<IActionResult> GetCount()
        {
            var count = await _service.GetCount();

            return Ok(new ResponseMessage(true, count, false));
        }

        [HttpGet]
        [Route("get-total-amount")]
        public async Task<IActionResult> GetTotalAmount()
        {
            return Ok(await _service.GetTotalAmountAsync());
        }

        [HttpGet]
        [Route("get-total-amount-month/{month}/{year}")]
        public async Task<IActionResult> GetTotalAmountMonth(int month, int year)
        {
            return Ok(await _service.GetTotalAmountMonthAsync(month, year));
        }
    }
}
