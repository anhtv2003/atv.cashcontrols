﻿using Asp.Versioning;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Services.Expenses;
using Atv.CashControls.Services.FileService;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Export")]
    [Route("/api/cash-controls/export")]
    public class ExportController : CashControlsController
    {
        private readonly IFileService _fileService;
        private readonly IExpenseService _expenseService;

        public ExportController(IFileService fileService, IExpenseService expenseService)
        {
            _fileService = fileService;
            _expenseService = expenseService;
        }

        [HttpGet]
        [Route("export-expense-to-excel")]
        public async Task<IActionResult> ExportToExcel()
        {
            var input = new GetExpensesInput()
            {
                MaxResultCount = 25,
                SkipCount = 25,
            };
            IEnumerable<ExpenseDto> expenses = new List<ExpenseDto>();

            var res = await _expenseService.GetListAync(input);

            if (res != null && res.TotalCount > 0)
            {

            }

            return Ok(await _fileService.ExpenseExportExcelSpreadsheetDocument(res.Items.AsEnumerable()));
        }
    }
}
