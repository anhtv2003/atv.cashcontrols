﻿using Asp.Versioning;
using Atv.CashControls.Dtos.Dashboard;
using Atv.CashControls.Services.Dashboard;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("dashboard")]
    [Route("/api/cash-controls/dashboard")]
    public class DashboardController : CashControlsController
    {
        private readonly IDashboardService _dashboardService;

        public DashboardController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        [HttpGet]
        [Route("get-license-statistic-widget")]
        public async Task<IActionResult> GetLicenseStatisticWidgetAsync(DateTime startDate, DateTime endDate)
        {
            var result = await _dashboardService.GetLicenseStatisticWidgetAsync(new Dtos.Dashboard.LicenseStatisticWidgetInputDto
            {
                StartDate = startDate,
                EndDate = endDate
            });

            return Ok(result);
        }

        [HttpGet]
        [Route("get-newuser-statistic-widget")]
        public async Task<IActionResult> GetNewUserStatisticWidgetAsync(DateTime startDate, DateTime endDate, NewUserStatisticFrequency frequency)
        {
            var result = await _dashboardService.GetNewUserStatisticWidgetAsync(new Dtos.Dashboard.NewUserStatisticWidgetInputDto
            {
                StartDate = startDate,
                EndDate = endDate,
                Frequency = frequency
            });

            return Ok(result);
        }

        [HttpGet]
        [Route("get-day-expenses-statistic-widget")]
        public async Task<IActionResult> GetDayExpenseStatisticWidgetAsync(int month, int year)
        {
            var result = await _dashboardService.GetDayExpenseStatisticWidgetAsync(new DayExpenseStatisticWidgetInputDto
            {
                Month = month,
                Year = year
            });

            return Ok(result);
        }

        [HttpGet]
        [Route("category-expense-statistic-widget")]
        public async Task<IActionResult> CategoryExpenseStatisticWidgetAsync(int month, int year)
        {
            var result = await _dashboardService.CategoryExpenseStatisticWidgetAsync(new CategoryExpenseStatisticWidgetInputDto
            {
                Month = month,
                Year = year
            });

            return Ok(result);
        }
    }
}
