using Asp.Versioning;
using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Services.BankingNotes;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("BankingTransactions")]
    [Route("/api/cash-controls/banking-transactions")]
    public class BankingTransactionsController : CashControlsController
    {
        private readonly IBankingTransactionService _bankingTransactionService;
        public BankingTransactionsController(IBankingTransactionService bankingTransactionService)
        {
            _bankingTransactionService = bankingTransactionService;
        }

        [HttpGet]
        [Route("transactions-list")]
        public async Task<PagedResultDto<BankingNoteTransactionDto>> GetList(GetBankingNoteTransactionInput input)
        {
            return await _bankingTransactionService.GetListAync(input);
        }

        [HttpPost]
        [Route("new-transaction")]
        public async Task<IActionResult> Create(CreateBankingNoteTransactionDto input)
        {
            var res = await _bankingTransactionService.CreateAsync(input);
            
            return Ok(res);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task DeleteAsync(Guid id)
        {
            return _bankingTransactionService.DeleteAsync(id);
        }
    }
}