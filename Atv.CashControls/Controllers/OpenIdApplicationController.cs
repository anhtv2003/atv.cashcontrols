﻿using Asp.Versioning;
using Atv.CashControls.Services.OpenIdApplications;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("OpenIdApplication")]
    [Route("api/cash-controls/openid/app")]
    public class OpenIdApplicationController : CashControlsController, IOpenIdApplicationAppService
    {
        private readonly IOpenIdApplicationAppService _openIdApplicationAppService;

        public OpenIdApplicationController(IOpenIdApplicationAppService openIdApplicationAppService)
        {
            _openIdApplicationAppService = openIdApplicationAppService;
        }

        [HttpPost]
        [Route("add")]
        public Task CreateAsync(CreateApplicationInput input)
        {
            return _openIdApplicationAppService.CreateAsync(input);
        }
    }
}
