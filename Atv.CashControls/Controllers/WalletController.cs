﻿using Asp.Versioning;
using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Dtos.Wallets;
using Atv.CashControls.Services.BankingNotes;
using Atv.CashControls.Services.Wallets;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Wallets")]
    [Route("/api/cash-controls/wallets")]
    public class WalletController : CashControlsController
    {
        private readonly IWalletService _walletService;
        private readonly IBankingNoteService _bankingNoteService;

        public WalletController(IWalletService walletService, IBankingNoteService bankingNoteService)
        {
            _walletService = walletService;
            _bankingNoteService = bankingNoteService;
        }

        [HttpGet]
        [Route("wallets-list")]
        public async Task<PagedResultDto<WalletDto>> GetListAsync(GetWalletInput input)
        {
            var wallets = await _walletService.GetListAync(input);

            return wallets;
        }

        [HttpGet]
        [Route("get-by-id")]
        public async Task<IActionResult> GetByIdAsync (Guid id)
        {
            return Ok(await _walletService.GetByIdAsync(id));
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task DeleteAsync(Guid id)
        {
            return _walletService.DeleteAsync(id);
        }

        [HttpPut]
        [Route("change-money/{id}/{method}")]
        public async Task<IActionResult> ChangeMoney(Guid id, string method, [FromBody] decimal amount)
        {
            var res = await _walletService.ChangeMoney(id, amount, method);

            return Ok(res);
        }
    }
}
