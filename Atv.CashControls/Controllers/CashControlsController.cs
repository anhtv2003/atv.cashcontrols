﻿using Atv.CashControls.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Atv.CashControls.Controllers
{
    public class CashControlsController : AbpController
    {
        public CashControlsController()
        {
            LocalizationResource = typeof(CashControlsResource);
        }
    }
}
