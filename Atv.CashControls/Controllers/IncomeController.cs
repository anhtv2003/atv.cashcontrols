﻿using Asp.Versioning;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Dtos.Incomes;
using Atv.CashControls.Services.CashCategories;
using Atv.CashControls.Services.Incomes;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Users;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Incomes")]
    [Route("api/cash-controls/incomes")]
    public class IncomeController : CashControlsController
    {
        private readonly IIncomeService _service;
        private readonly IMapper _mapper;

        public IncomeController(IIncomeService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task DeleteAsync(Guid id)
        {
            return _service.DeleteAsync(id);
        }

        [HttpGet]
        [Route("get-by-id/{id}")]
        public async Task<IncomeDto> GetAsync(Guid id)
        {
            return await _service.GetAsync(id);
        }

        [HttpGet]
        [Route("get-list")]
        public async Task<PagedResultDto<IncomeDto>> GetListAync(GetIncomeInput input)
        {
            return await _service.GetListAync(input);
        }
    }
}
