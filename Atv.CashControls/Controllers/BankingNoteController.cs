﻿using Asp.Versioning;
using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Services.BankingNotes;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("BankingNotes")]
    [Route("/api/cash-controls/banking-notes")]
    public class BankingNoteController : CashControlsController
    {
        private readonly IBankingNoteService _bankingNoteService;

        public BankingNoteController(IBankingNoteService bankingNoteService)
        {
            _bankingNoteService = bankingNoteService;
        }

        [HttpGet]
        [Route("banking-notes-list")]
        public async Task<PagedResultDto<BankingNoteDto>> GetListAsync(GetBankingNoteInput input)
        {
            return await _bankingNoteService.GetListAync(input);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task DeleteAsync(Guid id)
        {
            return _bankingNoteService.DeleteAsync(id);
        }

        [HttpGet]
        [Route("get-money/{bankingId}")]
        public async Task<IActionResult> GetMoney(Guid bankingId)
        {
            return Ok(await _bankingNoteService.GetMoneyOfBanking(bankingId));
        }

        [HttpPost]
        [Route("update-money-transaction/{bankingNoteId}")]
        public async Task UpdateMoneyTransaction(Guid bankingNoteId, decimal price, string method)
        {
            await _bankingNoteService.UpdateMoneyWhenCreateTransaction(bankingNoteId, price, method);
        }
    }
}
