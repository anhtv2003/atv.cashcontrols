﻿using Asp.Versioning;
using Atv.CashControls.Dtos.Depts;
using Atv.CashControls.Services.Depts;
using Autofac.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Depts")]
    [Route("/api/cash-controls/depts")]
    public class DeptController : CashControlsController
    {
        private readonly IDeptService _deptService;

        public DeptController(IDeptService deptService)
        {
            _deptService = deptService;
        }

        [HttpGet]
        [Route("dept-list")]
        public async Task<PagedResultDto<DeptsDto>> GetListAsync(GetDeptsInput input)
        {
            return await _deptService.GetListAync(input);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task DeleteAsync(Guid id)
        {
            return _deptService.DeleteAsync(id);
        }
    }
}
