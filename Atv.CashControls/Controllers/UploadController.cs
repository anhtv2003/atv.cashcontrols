﻿using Asp.Versioning;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Upload")]
    [Route("/api/cash-controls/upload")]
    public class UploadController : CashControlsController
    {
        [HttpPost]
        public async Task<IActionResult> Upload()
        {
            return Ok("");
        }
    }
}
