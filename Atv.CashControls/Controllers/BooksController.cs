using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asp.Versioning;
using Atv.CashControls.Dtos.Books;
using Atv.CashControls.Services.Books;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Books")]
    [Route("/api/cash-controls/books")]
    public class BooksController : CashControlsController
    {
        private readonly IBookAppService _bookAppService;

        public BooksController(IBookAppService bookAppService)
        {
            _bookAppService = bookAppService;
        }

        [HttpGet]
        [Route("books-list")]
        public async Task<PagedResultDto<BookDto>> GetListAsync(BookGetListInput input)
        {
            return await _bookAppService.GetListAync(input);
        }
    }
}