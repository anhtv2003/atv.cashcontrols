﻿using Asp.Versioning;
using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Commons;
using Atv.CashControls.Services.CashCategories;
using Autofac.Core;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Content;
using Volo.Abp.Identity;
using Volo.Abp.Users;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("CashCategories")]
    [Route("api/cash-controls/cash-categories")]
    public class CashcategoryController : CashControlsController
    {
        private readonly ICashCategoryService _service;
        private readonly ICurrentUser _currentUser;
        private readonly IMapper _mapper;

        public CashcategoryController(ICashCategoryService service, ICurrentUser currentUser, IMapper mapper)
        {
            _service = service;
            _currentUser = currentUser;
            _mapper = mapper;
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task<ResponseMessage> DeleteAsync(Guid id)
        {
            return _service.DeleteAsync(id);
        }

        [HttpGet]
        [Route("get-by-id/{id}")]
        public Task<CashCategoryDto> GetAsync(Guid id)
        {
            return _service.GetAsync(id);
        }

        //[HttpGet]
        //[Route("get-download-token")]
        //public Task<DownloadTokenResultDto> GetDownloadTokenAsync()
        //{
        //    return _service.GetDownloadTokenAsync();
        //}

        //[HttpGet]
        //[Route("get-file")]
        //public Task<IRemoteStreamContent> GetFileAsync(CashCategoryDownloadDto input)
        //{
        //    return _service.GetFileAsync(input);
        //}

        [HttpGet]
        [Route("get-list")]
        public Task<PagedResultDto<CashCategoryDto>> GetListAync(GetCashCategoriesInput input)
        {
            return _service.GetListAync(input);
        }

        [HttpGet]
        [Route("get-list-no-paging")]
        public Task<List<CashCategoryDto>> GetListNoPaging()
        {
            return _service.GetListAync();
        }
    }
}
