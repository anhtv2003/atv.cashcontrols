﻿using Asp.Versioning;
using Atv.CashControls.Dtos.Loans;
using Atv.CashControls.Services.Loans;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Controllers
{
    [RemoteService(Name = "CashControls")]
    [Area("CashControls")]
    [ControllerName("Loans")]
    [Route("/api/cash-controls/loans")]
    public class LoansController : CashControlsController
    {
        private readonly ILoanService _loanService;

        public LoansController(ILoanService loanService)
        {
            _loanService = loanService;
        }

        [HttpGet]
        [Route("loans-list")]
        public async Task<PagedResultDto<LoansDto>> GetListAsync(GetLoansInput input)
        {
            return await _loanService.GetListAync(input);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public Task DeleteAsync(Guid id)
        {
            return _loanService.DeleteAsync(id);
        }
    }
}
