﻿using Atv.CashControls.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace Atv.CashControls.Permissions
{
    public class CashControlsPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(CashControlsPermissions.GroupName);

            var cashCategoryPermissions = myGroup.AddPermission(CashControlsPermissions.CashCategories.Default, L("Permission:CashCategories"));
            cashCategoryPermissions.AddChild(CashControlsPermissions.CashCategories.Create, L("Permission:Create"));
            cashCategoryPermissions.AddChild(CashControlsPermissions.CashCategories.Edit, L("Permission:Edit"));
            cashCategoryPermissions.AddChild(CashControlsPermissions.CashCategories.Delete, L("Permission:Delete"));

            var expensePermissions = myGroup.AddPermission(CashControlsPermissions.Expenses.Default, L("Permission:Expenses"));
            expensePermissions.AddChild(CashControlsPermissions.Expenses.Create, L("Permission:Create"));
            expensePermissions.AddChild(CashControlsPermissions.Expenses.Edit, L("Permission:Edit"));
            expensePermissions.AddChild(CashControlsPermissions.Expenses.Delete, L("Permission:Delete"));

            var deptPermissions = myGroup.AddPermission(CashControlsPermissions.Depts.Default, L("Permission:Depts"));
            deptPermissions.AddChild(CashControlsPermissions.Depts.Create, L("Permission:Create"));
            deptPermissions.AddChild(CashControlsPermissions.Depts.Edit, L("Permission:Edit"));
            deptPermissions.AddChild(CashControlsPermissions.Depts.Delete, L("Permission:Delete"));

            var loanPermissions = myGroup.AddPermission(CashControlsPermissions.Loans.Default, L("Permission:Loans"));
            loanPermissions.AddChild(CashControlsPermissions.Loans.Create, L("Permission:Create"));
            loanPermissions.AddChild(CashControlsPermissions.Loans.Edit, L("Permission:Edit"));
            loanPermissions.AddChild(CashControlsPermissions.Loans.Delete, L("Permission:Delete"));

            var bankingNotePermissions = myGroup.AddPermission(CashControlsPermissions.BankingNotes.Default, L("Permission:BankingNotes"));
            bankingNotePermissions.AddChild(CashControlsPermissions.BankingNotes.Create, L("Permission:Create"));
            bankingNotePermissions.AddChild(CashControlsPermissions.BankingNotes.Edit, L("Permission:Edit"));
            bankingNotePermissions.AddChild(CashControlsPermissions.BankingNotes.Delete, L("Permission:Delete"));

            var walletPermissions = myGroup.AddPermission(CashControlsPermissions.Wallets.Default, L("Permission:Wallets"));
            walletPermissions.AddChild(CashControlsPermissions.Wallets.Create, L("Permission:Create"));
            walletPermissions.AddChild(CashControlsPermissions.Wallets.Edit, L("Permission:Edit"));
            walletPermissions.AddChild(CashControlsPermissions.Wallets.Delete, L("Permission:Delete"));

            var incomePermissions = myGroup.AddPermission(CashControlsPermissions.Incomes.Default, L("Permission:Incomes"));
            incomePermissions.AddChild(CashControlsPermissions.Incomes.Create, L("Permission:Create"));
            incomePermissions.AddChild(CashControlsPermissions.Incomes.Edit, L("Permission:Edit"));
            incomePermissions.AddChild(CashControlsPermissions.Incomes.Delete, L("Permission:Delete"));

            // Demo thôi ní
            var bsGroup = context.AddGroup(CashControlsPermissions.BookStoreGroupName);

            var bsPermissions = bsGroup.AddPermission(CashControlsPermissions.BookStores.Default, L("Permission:BookStores"));
            bsPermissions.AddChild(CashControlsPermissions.BookStores.Create, L("Permission:Create"));
            bsPermissions.AddChild(CashControlsPermissions.BookStores.Edit, L("Permission:Edit"));
            bsPermissions.AddChild(CashControlsPermissions.BookStores.Delete, L("Permission:Delete"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<CashControlsResource>(name);
        }
    }
}
