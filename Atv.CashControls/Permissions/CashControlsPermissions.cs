﻿namespace Atv.CashControls.Permissions
{
    public class CashControlsPermissions
    {
        public const string GroupName = "CashControls";
        public const string BookStoreGroupName = "BookStores";

        public static class BookStores
        {
            public const string Default = BookStoreGroupName + ".BookStore";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public static class CashCategories
        {
            public const string Default = GroupName + ".CashCategories";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public static class Expenses
        {
            public const string Default = GroupName + ".Expenses";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public static class Depts
        {
            public const string Default = GroupName + ".Depts";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public static class Loans
        {
            public const string Default = GroupName + ".Loans";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public static class BankingNotes
        {
            public const string Default = GroupName + ".BankingNotes";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public static class Wallets
        {
            public const string Default = GroupName + ".Wallets";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }

        public static class Incomes
        {
            public const string Default = GroupName + ".Incomes";
            public const string Edit = Default + ".Edit";
            public const string Create = Default + ".Create";
            public const string Delete = Default + ".Delete";
        }
    }
}
