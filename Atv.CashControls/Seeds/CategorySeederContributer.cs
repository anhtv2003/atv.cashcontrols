using System.Text;
using Atv.CashControls.Entities;
using Newtonsoft.Json;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Seeds
{
    public class CategorySeederContributer : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<CashCategory, Guid> _repository;
        public CategorySeederContributer(IRepository<CashCategory, Guid> repository)
        {
            _repository = repository;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (!await _repository.AnyAsync())
            {
                await _repository.InsertManyAsync(GetInitialCategory());
            }
        }

        private CashCategory[] GetInitialCategory()
        {
            var json = GetEmbeddedResourceAsText("Atv.CashControls.Seeds._CashCategories_.json");

            return JsonConvert.DeserializeObject<CashCategory[]>(json);
        }

        private string GetEmbeddedResourceAsText(string nameWithNamespace)
        {
            using var stream = GetType().Assembly.GetManifestResourceStream(nameWithNamespace);

            if (stream == null)
            {
                throw new Exception($"Resource '{nameWithNamespace}' not found.");
            }

            return Encoding.UTF8.GetString(stream.GetAllBytes());
        }
    }
}