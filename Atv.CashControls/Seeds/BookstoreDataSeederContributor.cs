using System.Text;
using Atv.CashControls.Entities;
using Newtonsoft.Json;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Seeds
{
    public class BookstoreDataSeederContributor : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<Book, Guid> _repository;

        public BookstoreDataSeederContributor(IRepository<Book, Guid> repository)
        {
            _repository = repository;
        }

        public async Task SeedAsync(DataSeedContext context)
        {
            if (!await _repository.AnyAsync())
            {
                await _repository.InsertManyAsync(GetInitialBooks());
            }
        }

        private Book[] GetInitialBooks()
        {
            var json = GetEmbeddedResourceAsText("Atv.CashControls.Seeds.initial-books.json");

            return JsonConvert.DeserializeObject<Book[]>(json);
        }

        private string GetEmbeddedResourceAsText(string nameWithNamespace)
        {
            using var stream = GetType().Assembly.GetManifestResourceStream(nameWithNamespace);

            if (stream == null)
            {
                throw new Exception($"Resource '{nameWithNamespace}' not found.");
            }

            return Encoding.UTF8.GetString(stream.GetAllBytes());
        }
    }
}