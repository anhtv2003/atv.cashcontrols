﻿using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.Expenses;
using Atv.CashControls.Repositories.Wallets;
using AutoMapper.Internal.Mappers;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.Expenses
{
    public class ExpenseService : ApplicationService, IExpenseService
    {
        private readonly IExpenseRepository _repository;
        private readonly IWalletRepository _walletRepository;

        public ExpenseService(IExpenseRepository repository, IWalletRepository walletRepository)
        {
            _repository = repository;
            _walletRepository = walletRepository;
        }

        public async Task<ExpenseDto> CreateAsync(CreateExpenseDto input)
        {
            var result = ObjectMapper.Map<CreateExpenseDto, Expense>(input);
            result = await _repository.InsertAsync(result, true);
            return ObjectMapper.Map<Expense, ExpenseDto>(result);
        }

        public async Task DeleteAsync(Guid id)
        {
            var expense = await _repository.GetAsync(id);

            if (expense != null)
            {
                await _repository.DeleteAsync(id);

                if (expense.WalletId.HasValue)
                {
                    await _walletRepository.ChangeMoney(expense.WalletId.Value, expense.Price, "add");
                }
            }
        }

        public async Task<ExpenseDto> GetAsync(Guid id)
        {
            var result = await _repository.GetAsync(id);
            return ObjectMapper.Map<Expense, ExpenseDto>(result);
        }

        public async Task<long> GetCount()
        {
            var count = await _repository.GetCountAsync();

            return count;
        }

        public async Task<PagedResultDto<ExpenseDto>> GetListAync(GetExpensesInput input)
        {
            var totalCount = await _repository.GetCountAsync(input.FilterText, input.Date, input.CategoryID);
            var items = await _repository.GetListAsync(input.FilterText, input.Date, input.CategoryID, input.Status, input.Sorting, input.MaxResultCount, input.SkipCount, new[] { "CashCategory" });

            return new PagedResultDto<ExpenseDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Expense>, List<ExpenseDto>>(items.ToList())
            };
        }

        public async Task<decimal> GetTotalAmountAsync()
        {
            return await _repository.GetTotalAmountAsync();
        }

        public async Task<decimal> GetTotalAmountMonthAsync(int month, int year)
        {
            return await _repository.GetTotalAmountMonthAsync(month, year);
        }

        public async Task<ExpenseDto> UpdateAsync(Guid id, UpdateExpenseDto input)
        {
            var queryable = await _repository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var result = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, result);
            result = await _repository.UpdateAsync(result);
            return ObjectMapper.Map<Expense, ExpenseDto>(result);
        }
    }
}
