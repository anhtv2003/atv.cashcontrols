﻿using Atv.CashControls.Dtos.Expenses;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Services.Expenses
{
    public interface IExpenseService
    {
        Task<PagedResultDto<ExpenseDto>> GetListAync(GetExpensesInput input);
        Task<ExpenseDto> GetAsync(Guid id);
        Task<ExpenseDto> CreateAsync(CreateExpenseDto input);
        Task<ExpenseDto> UpdateAsync(Guid id, UpdateExpenseDto input);
        Task DeleteAsync(Guid id);
        Task<long> GetCount();
        Task<decimal> GetTotalAmountAsync();
        Task<decimal> GetTotalAmountMonthAsync(int month, int year);
    }
}
