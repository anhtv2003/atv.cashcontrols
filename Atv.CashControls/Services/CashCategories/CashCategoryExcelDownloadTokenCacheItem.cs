﻿namespace Atv.CashControls.Services.CashCategories
{
    public class CashCategoryExcelDownloadTokenCacheItem
    {
        public string Token { get; set; }
    }
}
