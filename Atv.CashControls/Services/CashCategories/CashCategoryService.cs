﻿using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Commons;
using Atv.CashControls.Entities;
using Atv.CashControls.Permissions;
using Atv.CashControls.Repositories.CashCategories;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Caching;

namespace Atv.CashControls.Services.CashCategories
{
    [Authorize(CashControlsPermissions.CashCategories.Default)]
    public class CashCategoryService : ApplicationService, ICashCategoryService
    {
        private readonly ICashCategoryRepository _repository;
        private readonly IDistributedCache<CashCategoryExcelDownloadTokenCacheItem, string> _excelDownloadContentCache;
        //private readonly IExporterManager _exporterManager;

        public CashCategoryService(ICashCategoryRepository repository,
                                    IDistributedCache<CashCategoryExcelDownloadTokenCacheItem, string> excelDownloadContentCache
            // IExporterManager exporterManager
            )
        {
            _repository = repository;
            _excelDownloadContentCache = excelDownloadContentCache;
            //_exporterManager = exporterManager;
        }

        [Authorize(CashControlsPermissions.CashCategories.Create)]
        public async Task<CashCategoryDto> CreateAsync(CreateCashCategoryDto input)
        {
            var cashCateg = ObjectMapper.Map<CreateCashCategoryDto, CashCategory>(input);
            cashCateg = await _repository.InsertAsync(cashCateg);
            return ObjectMapper.Map<CashCategory, CashCategoryDto>(cashCateg);
        }

        [Authorize(CashControlsPermissions.CashCategories.Delete)]
        public async Task<ResponseMessage> DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);

            return new ResponseMessage
            {
                Success = true,
                Error = false
            };
        }

        public async Task<CashCategoryDto> GetAsync(Guid id)
        {
            var result = await _repository.GetAsync(id);
            return ObjectMapper.Map<CashCategory, CashCategoryDto>(result);
        }

        //public async Task<DownloadTokenResultDto> GetDownloadTokenAsync()
        //{
        //    var token = Guid.NewGuid().ToString("N");

        //    await _excelDownloadContentCache.SetAsync(
        //            token, 
        //            new CashCategoryExcelDownloadTokenCacheItem { Token = token },
        //            new Microsoft.Extensions.Caching.Distributed.DistributedCacheEntryOptions
        //            {
        //                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30),
        //            });
        //    return new DownloadTokenResultDto
        //    {
        //        Token = token,
        //    };
        //}

        //[AllowAnonymous]
        //public async Task<IRemoteStreamContent> GetFileAsync(CashCategoryDownloadDto input)
        //{
        //    var downloadToken = await _excelDownloadContentCache.GetAsync(input.DownloadToken);

        //    if (downloadToken == null || input.DownloadToken != downloadToken.Token)
        //    {
        //        throw new AbpAuthorizationException("Invalid download token: ", input.DownloadToken);
        //    }

        //    var items = await _repository.GetListAsync(input.FilterText, input.Name);
        //    var datas = (from item in items
        //                 let json = JsonConvert.SerializeObject(item)
        //                 let tmp = JsonConvert.DeserializeObject<IDictionary<string, object>>(json)
        //                 select tmp).ToList();

        //    var memoryStream = new MemoryStream();
        //    var fileName = string.Empty;
        //    if (input.FileType.Equals("CSV"))
        //    {
        //        memoryStream = await _exporterManager.GetStreamDocument(ExportCsvType.GetExportType(), datas, new OptionCsv
        //        {
        //            TempFolderPath = Path.GetTempPath()
        //        });

        //        fileName = "Cashcategory.csv";
        //        memoryStream.Seek(0, SeekOrigin.Begin);
        //    }
        //    else if (input.FileType.Equals("EXCEl"))
        //    {
        //        memoryStream = await _exporterManager.GetStreamDocument(ExportExcelType.GetExportType(), datas, new OptionExcel
        //        {
        //            NumberRowPerSheet = 500000
        //        });
        //        fileName = "CashCategory.xlsx";
        //        memoryStream.Seek(0, SeekOrigin.Begin);

        //    }

        //    return new RemoteStreamContent(memoryStream, fileName);
        //}

        public async Task<PagedResultDto<CashCategoryDto>> GetListAync(GetCashCategoriesInput input)
        {
            var totalCount = await _repository.GetCountAsync(input.FilterText, input.Name);
            var items = await _repository.GetListAsync(input.FilterText, input.Name, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<CashCategoryDto>()
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<CashCategory>, List<CashCategoryDto>>(items)
            };
        }

        public async Task<List<CashCategoryDto>> GetListAync()
        {
            return ObjectMapper.Map<List<CashCategory>, List<CashCategoryDto>>(await _repository.GetList());
        }

        [Authorize(CashControlsPermissions.CashCategories.Edit)]
        public async Task<CashCategoryDto> UpdateAsync(Guid id, UpdateCashCategoryDto input)
        {
            var queryable = await _repository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var cashCateg = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, cashCateg);
            cashCateg = await _repository.UpdateAsync(cashCateg);
            return ObjectMapper.Map<CashCategory, CashCategoryDto>(cashCateg);
        }
    }
}
