﻿using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Commons;
using Atv.CashControls.Entities;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Content;

namespace Atv.CashControls.Services.CashCategories
{
    public interface ICashCategoryService : IApplicationService
    {
        Task<PagedResultDto<CashCategoryDto>> GetListAync(GetCashCategoriesInput input);
        Task<List<CashCategoryDto>> GetListAync();
        Task<CashCategoryDto> GetAsync(Guid id);
        Task<CashCategoryDto> CreateAsync(CreateCashCategoryDto input);
        Task<CashCategoryDto> UpdateAsync(Guid id, UpdateCashCategoryDto input);
        Task<ResponseMessage> DeleteAsync(Guid id);
        //Task<DownloadTokenResultDto> GetDownloadTokenAsync();
        //Task<IRemoteStreamContent> GetFileAsync(CashCategoryDownloadDto input);
    }
}
