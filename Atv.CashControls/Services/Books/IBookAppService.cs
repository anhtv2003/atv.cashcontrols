using Atv.CashControls.Dtos.Books;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.Books
{
    public interface IBookAppService
    {
        Task<PagedResultDto<BookDto>> GetListAync(BookGetListInput input);
        Task<BookDto> GetAsync(Guid id);
        // Task<BookDto> CreateAsync(CreateBookDto input);
        // Task<BookDto> UpdateAsync(Guid id, UpdateBookDto input);
        Task DeleteAsync(Guid id);
        Task<long> GetCount();
    }
}