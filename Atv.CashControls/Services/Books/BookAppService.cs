using Atv.CashControls.Dtos.Books;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.Books;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.Books
{
    public class BookAppService : ApplicationService, IBookAppService
    {
        private readonly IBookRepository _bookRepository;

        public BookAppService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public async Task DeleteAsync(Guid id)
        {
            await _bookRepository.DeleteAsync(id);
        }

        public async Task<BookDto> GetAsync(Guid id)
        {
            var res = await _bookRepository.GetAsync(id);

            return ObjectMapper.Map<Book, BookDto>(res);
        }

        public async Task<long> GetCount()
        {
            return await _bookRepository.GetCountAsync();
        }

        public async Task<PagedResultDto<BookDto>> GetListAync(BookGetListInput input)
        {
            var totalCount = await _bookRepository.GetCountAsync(input.Filter, input.TotalPage, input.Year);

            var items = await _bookRepository.GetListAsync(input.Filter, input.TotalPage, input.Year, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<BookDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Book>, List<BookDto>>(items.ToList())
            };
        }
    }
}