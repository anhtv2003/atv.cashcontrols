﻿using Atv.CashControls.Entities;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using Volo.Abp.Application.Services;
using Atv.CashControls.Dtos.Expenses;

namespace Atv.CashControls.Services.FileService
{
    public class FileService : ApplicationService, IFileService
    {
        public Task<FileStreamResult> ExpenseExportExcelEpplus(IEnumerable<ExpenseDto> expenses, string fileName)
        {
            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                var worksheet = package.Workbook.Worksheets.Add("Expenses");
            }

            return null;
        }

        public async Task<byte[]> ExpenseExportExcelSpreadsheetDocument(IEnumerable<ExpenseDto> expenses)
        {
            var memoryStream = new MemoryStream();

            using var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook);
            var workbookPart = document.AddWorkbookPart();
            workbookPart.Workbook = new Workbook();

            var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new SheetData());

            var sheets = workbookPart.Workbook.AppendChild(new Sheets());

            sheets.AppendChild(new Sheet
            {
                Id = workbookPart.GetIdOfPart(worksheetPart),
                SheetId = 1,
                Name = "Expense"
            });

            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

            // Thêm tiêu đề cột
            var heederRow = new Row();
            heederRow.Append(
                new Cell { CellValue = new CellValue("Tên (Name)"), DataType = CellValues.String },
                new Cell { CellValue = new CellValue("Giá tiền (Price) VNĐ"), DataType = CellValues.String },
                new Cell { CellValue = new CellValue("Số lương (Quantity)"), DataType = CellValues.String },
                new Cell { CellValue = new CellValue("Phân loại (Category)"), DataType = CellValues.String },
                new Cell { CellValue = new CellValue("Ngày (Date)"), DataType = CellValues.String },
                new Cell { CellValue = new CellValue("Người tạo (Creator)"), DataType = CellValues.String },
                new Cell { CellValue = new CellValue("Trạng thái (Status)"), DataType = CellValues.String }
                );
            sheetData?.AppendChild(heederRow);

            foreach (var item in expenses)
            {
                var row = new Row();
                row.Append(
                    new Cell { CellValue = new CellValue(item.Name), DataType = CellValues.String },
                    new Cell { CellValue = new CellValue(item.Price.ToString()), DataType = CellValues.String },
                    new Cell { CellValue = new CellValue(item.Quantity.ToString()), DataType = CellValues.String },
                    new Cell { CellValue = new CellValue(item.CategoryName), DataType = CellValues.String },
                    new Cell { CellValue = new CellValue(item.Date.ToString("dd/MM/yyyy")), DataType = CellValues.String },
                    new Cell { CellValue = new CellValue(item.CreatorId.ToString()), DataType = CellValues.String },
                    new Cell { CellValue = new CellValue(item.Status.ToString()), DataType = CellValues.String }
                );
                sheetData?.AppendChild(row);
            }

            // Thiết lập các cột để tự động điều chỉnh chiều rộng
            var columns = new Columns();
            for (uint i = 1; i <= expenses.Count(); i++)
            {
                columns.Append(new Column
                {
                    Min = i,
                    Max = i,
                    Width = 20,  // Chỉnh chiều rộng ban đầu, có thể thay đổi nếu cầnz`
                    CustomWidth = true
                });
            }
            worksheetPart.Worksheet.InsertAt(columns, 0);

            //workbookPart.Workbook.Save();
            document?.Save();

            return memoryStream.ToArray();
        }

        public async Task<byte[]> ExportToExcelDemo()
        {
            var memoryStream = new MemoryStream();

            using var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook);
            var workbookPart = document.AddWorkbookPart();
            workbookPart.Workbook = new Workbook();

            var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new SheetData());

            var sheets = workbookPart.Workbook.AppendChild(new Sheets());

            sheets.AppendChild(new Sheet
            {
                Id = workbookPart.GetIdOfPart(worksheetPart),
                SheetId = 1,
                Name = "Sheet 1"
            });

            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

            var row1 = new Row();
            row1.AppendChild(
                new Cell
                {
                    CellValue = new CellValue("Abp Framework"),
                    DataType = CellValues.String
                }
            );
            sheetData?.AppendChild(row1);

            var row2 = new Row();
            row2.AppendChild(
                new Cell
                {
                    CellValue = new CellValue("Open Source"),
                    DataType = CellValues.String
                }
            );
            sheetData?.AppendChild(row2);

            var row3 = new Row();
            row3.AppendChild(
                new Cell
                {
                    CellValue = new CellValue("WEB APPLICATION FRAMEWORK"),
                    DataType = CellValues.String
                }
            );
            sheetData?.AppendChild(row3);

            document.Save();

            return memoryStream.ToArray();
        }

    }
}
