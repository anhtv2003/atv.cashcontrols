﻿using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Atv.CashControls.Services.FileService
{
    public interface IFileService
    {
        Task<FileStreamResult> ExpenseExportExcelEpplus(IEnumerable<ExpenseDto> expenses, string fileName);
        Task<byte[]> ExpenseExportExcelSpreadsheetDocument(IEnumerable<ExpenseDto> expenses);

        Task<byte[]> ExportToExcelDemo();
    }
}
