﻿using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Commons;
using Atv.CashControls.Dtos.Incomes;
using Atv.CashControls.Permissions;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.Incomes
{
    
    public interface IIncomeService : IApplicationService
    {
        Task<PagedResultDto<IncomeDto>> GetListAync(GetIncomeInput input);
        Task<IncomeDto> GetAsync(Guid id);
        Task<IncomeDto> CreateAsync(CreateIncomeDto input);
        Task<IncomeDto> UpdateAsync(Guid id, UpdateIncomeDto input);
        Task<ResponseMessage> DeleteAsync(Guid id);
    }
}
