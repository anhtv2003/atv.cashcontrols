﻿using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Commons;
using Atv.CashControls.Dtos.Incomes;
using Atv.CashControls.Entities;
using Atv.CashControls.Permissions;
using Atv.CashControls.Repositories.CashCategories;
using Atv.CashControls.Repositories.Incomes;
using Atv.CashControls.Services.CashCategories;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Caching;
using Volo.Abp.ObjectMapping;

namespace Atv.CashControls.Services.Incomes
{
    [Authorize(CashControlsPermissions.Incomes.Default)]
    public class IncomeService : ApplicationService, IIncomeService
    {
        private readonly IIncomeRepository _repository;

        public IncomeService(IIncomeRepository repository)
        {
            _repository = repository;
        }

        public async Task<IncomeDto> CreateAsync(CreateIncomeDto input)
        {
            var income = ObjectMapper.Map<CreateIncomeDto, Income>(input);
            income = await _repository.InsertAsync(income);
            return ObjectMapper.Map<Income, IncomeDto>(income);
        }

        public async Task<ResponseMessage> DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);

            return new ResponseMessage
            {
                Success = true,
                Error = false
            };
        }

        public async Task<IncomeDto> GetAsync(Guid id)
        {
            var result = await _repository.GetAsync(id);
            return ObjectMapper.Map<Income, IncomeDto>(result);
        }

        public async Task<PagedResultDto<IncomeDto>> GetListAync(GetIncomeInput input)
        {
            var totalCount = await _repository.GetCountAsync(input.FilterText);
            var items = await _repository.GetListAsync(input.FilterText, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<IncomeDto>()
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<Income>, List<IncomeDto>>(items)
            };
        }

        public async Task<IncomeDto> UpdateAsync(Guid id, UpdateIncomeDto input)
        {
            var queryable = await _repository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var items = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, items);
            items = await _repository.UpdateAsync(items);
            return ObjectMapper.Map<Income, IncomeDto>(items);
        }
    }
}
