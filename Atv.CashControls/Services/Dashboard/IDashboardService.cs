﻿using Atv.CashControls.Dtos.Dashboard;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.Dashboard
{
    public interface IDashboardService : IApplicationService
    {
        Task<DayExpenseStatisticWidgetResultDto> GetDayExpenseStatisticWidgetAsync(DayExpenseStatisticWidgetInputDto input);

        Task<CategoryExpenseStatisticWidgetResultDto> CategoryExpenseStatisticWidgetAsync(CategoryExpenseStatisticWidgetInputDto input);

        Task<CountersWidgetResultDto> GetCountersWidgetAsync(CountersWidgetInputDto input);

        Task<LicenseStatistiWidgetResultDto> GetLicenseStatisticWidgetAsync(LicenseStatisticWidgetInputDto input);

        Task<NewUserStatisticWidgetResultDto> GetNewUserStatisticWidgetAsync(NewUserStatisticWidgetInputDto input);
    }
}
