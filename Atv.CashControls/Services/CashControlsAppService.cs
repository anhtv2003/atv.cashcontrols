﻿using Atv.CashControls.Localization;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services;

/* Inherit your application services from this class. */
public abstract class CashControlsAppService : ApplicationService
{
    protected CashControlsAppService()
    {
        LocalizationResource = typeof(CashControlsResource);
    }
}