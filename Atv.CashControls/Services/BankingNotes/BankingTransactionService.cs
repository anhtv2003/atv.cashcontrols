using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.BankingNotes;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.BankingNotes
{
    public class BankingTransactionService : ApplicationService, IBankingTransactionService
    {
        private readonly IBankingTransactionRepository _bankingTransactionRepository;
        private readonly IBankingNoteRepository _bankingNoteRepository;

        public BankingTransactionService(IBankingTransactionRepository bankingTransactionRepository, IBankingNoteRepository bankingNoteRepository)
        {
            _bankingTransactionRepository = bankingTransactionRepository;
            _bankingNoteRepository = bankingNoteRepository;
        }

        public async Task<BankingNoteTransactionDto> CreateAsync(CreateBankingNoteTransactionDto input)
        {
            var isExist = await _bankingNoteRepository.GetAsync(input.BankingNoteId);

            if (isExist != null)
            {
                var entity = ObjectMapper.Map<CreateBankingNoteTransactionDto, BankingNoteTransaction>(input);

                entity = await _bankingTransactionRepository.InsertAsync(entity);

                return ObjectMapper.Map<BankingNoteTransaction, BankingNoteTransactionDto>(entity);
            }

            return null;
        }

        public async Task DeleteAsync(Guid id)
        {
            var res = await _bankingTransactionRepository.GetByIdAsync(id);

            if (res != null)
            {
                await _bankingTransactionRepository.DeleteAsync(id);

                await _bankingNoteRepository.UpdateMoneyWhenCreateTransaction(res.BankingNoteId, res.Amount, "add");
            }
        }

        public async Task<BankingNoteTransactionDto> GetAsync(Guid id, Guid bankingId)
        {
            return ObjectMapper.Map<BankingNoteTransaction, BankingNoteTransactionDto>(await _bankingTransactionRepository.GetByIdAsync(id, bankingId));
        }

        public Task<long> GetCount()
        {
            throw new NotImplementedException();
        }

        public async Task<PagedResultDto<BankingNoteTransactionDto>> GetListAync(GetBankingNoteTransactionInput input)
        {
            var totalCount = await _bankingTransactionRepository.GetCountAsync(input.Filter);

            var items = await _bankingTransactionRepository.GetListAsync(input.NoteId, input.Filter, null, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<BankingNoteTransactionDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<BankingNoteTransaction>, List<BankingNoteTransactionDto>>(items.ToList())
            };
        }

        public async Task<BankingNoteTransactionDto> UpdateAsync(Guid id, UpdateBankingNoteTransactionDto input)
        {
            var queryable = await _bankingTransactionRepository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var result = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, result);
            result = await _bankingTransactionRepository.UpdateAsync(result);
            return ObjectMapper.Map<BankingNoteTransaction, BankingNoteTransactionDto>(result);
        }
    }
}