using Atv.CashControls.Dtos.BankingNotes;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Services.BankingNotes
{
    public interface IBankingTransactionService
    {
        Task<PagedResultDto<BankingNoteTransactionDto>> GetListAync(GetBankingNoteTransactionInput input);
        Task<BankingNoteTransactionDto> GetAsync(Guid id, Guid bankingId);
        Task<BankingNoteTransactionDto> CreateAsync(CreateBankingNoteTransactionDto input);
        Task<BankingNoteTransactionDto> UpdateAsync(Guid id, UpdateBankingNoteTransactionDto input);
        Task DeleteAsync(Guid id);
        Task<long> GetCount();
    }
}