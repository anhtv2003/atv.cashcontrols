﻿using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.BankingNotes;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.BankingNotes
{
    public class BankingNoteService : ApplicationService, IBankingNoteService
    {
        private readonly IBankingNoteRepository _bankingNoteRepository;

        public BankingNoteService(IBankingNoteRepository bankingNoteRepository)
        {
            _bankingNoteRepository = bankingNoteRepository;
        }

        public async Task<BankingNoteDto> CreateAsync(CreateBankingNoteDto input)
        {
            var entity = ObjectMapper.Map<CreateBankingNoteDto, BankingNote>(input);
            entity = await _bankingNoteRepository.InsertAsync(entity);
            return ObjectMapper.Map<BankingNote, BankingNoteDto>(entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _bankingNoteRepository.DeleteAsync(id);
        }

        public async Task<BankingNoteDto> GetAsync(Guid id)
        {
            var res = await _bankingNoteRepository.GetAsync(id);

            return ObjectMapper.Map<BankingNote, BankingNoteDto>(res);
        }

        public async Task<long> GetCount()
        {
            return await _bankingNoteRepository.GetCountAsync();
        }

        public async Task<PagedResultDto<BankingNoteDto>> GetListAync(GetBankingNoteInput input)
        {
            var totalCount = await _bankingNoteRepository.GetCountAsync(input.FilterText);

            var items = await _bankingNoteRepository.GetListAsync(input.FilterText, input.Sorting, input.MaxResultCount, input.SkipCount);

            return new PagedResultDto<BankingNoteDto>
            {
                TotalCount = totalCount,
                Items = ObjectMapper.Map<List<BankingNote>, List<BankingNoteDto>>(items.ToList())
            };
        }

        public async Task<decimal> GetMoneyOfBanking(Guid bankingId)
        {
            return await _bankingNoteRepository.GetMoneyOfBanking(bankingId);
        }

        public async Task<BankingNoteDto> UpdateAsync(Guid id, UpdateBankingNoteDto input)
        {
            var queryable = await _bankingNoteRepository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var result = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, result);
            result = await _bankingNoteRepository.UpdateAsync(result);
            return ObjectMapper.Map<BankingNote, BankingNoteDto>(result);
        }

        public async Task UpdateMoneyWhenCreateTransaction(Guid bankingNoteId, decimal price, string method)
        {
            await _bankingNoteRepository.UpdateMoneyWhenCreateTransaction(bankingNoteId, price, method);
        }
    }
}
