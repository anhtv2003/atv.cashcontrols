﻿using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Dtos.Depts;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Services.BankingNotes
{
    public interface IBankingNoteService
    {
        Task<PagedResultDto<BankingNoteDto>> GetListAync(GetBankingNoteInput input);
        Task<BankingNoteDto> GetAsync(Guid id);
        Task<BankingNoteDto> CreateAsync(CreateBankingNoteDto input);
        Task<BankingNoteDto> UpdateAsync(Guid id, UpdateBankingNoteDto input);
        Task DeleteAsync(Guid id);
        Task<long> GetCount();
        Task<decimal> GetMoneyOfBanking(Guid bankingId);
        Task UpdateMoneyWhenCreateTransaction(Guid bankingNoteId, decimal price, string method);
    }
}
