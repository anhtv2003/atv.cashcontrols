﻿using Atv.CashControls.Dtos.Depts;
using Atv.CashControls.Dtos.Loans;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Services.Loans
{
    public interface ILoanService
    {
        Task<PagedResultDto<LoansDto>> GetListAync(GetLoansInput input);
        Task<LoansDto> GetAsync(Guid id);
        Task<LoansDto> CreateAsync(CreateLoansDto input);
        Task<LoansDto> UpdateAsync(Guid id, UpdateLoansDto input);
        Task DeleteAsync(Guid id);
        Task<long> GetCount();
    }
}
