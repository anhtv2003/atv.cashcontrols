﻿using Atv.CashControls.Dtos.Depts;
using Atv.CashControls.Dtos.Loans;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.Depts;
using Atv.CashControls.Repositories.Loans;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.Loans
{
    public class LoanService : ApplicationService, ILoanService
    {
        private readonly ILoanRepository _loanRepository;

        public LoanService(ILoanRepository loanRepository)
        {
            _loanRepository = loanRepository;
        }

        public async Task<LoansDto> CreateAsync(CreateLoansDto input)
        {
            var entity = ObjectMapper.Map<CreateLoansDto, Loan>(input);
            entity = await _loanRepository.InsertAsync(entity);
            return ObjectMapper.Map<Loan, LoansDto>(entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _loanRepository.DeleteAsync(id);
        }

        public async Task<LoansDto> GetAsync(Guid id)
        {
            var entity = await _loanRepository.GetAsync(id);

            return ObjectMapper.Map<Loan, LoansDto>(entity);
        }

        public async Task<long> GetCount()
        {
            return await _loanRepository.GetCountAsync();
        }

        public async Task<PagedResultDto<LoansDto>> GetListAync(GetLoansInput input)
        {
            var count = await _loanRepository.GetCountAsync(input.FilterText, input.FromDate, input.ToDate);

            var items = await _loanRepository.GetListAsync(input.FilterText, input.FromDate, input.ToDate, input.Sorting, input.MaxResultCount, input.SkipCount, input.Status.Value);

            return new PagedResultDto<LoansDto>
            {
                TotalCount = count,
                Items = ObjectMapper.Map<List<Loan>, List<LoansDto>>(items.ToList())
            };
        }

        public async Task<LoansDto> UpdateAsync(Guid id, UpdateLoansDto input)
        {
            var queryable = await _loanRepository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var result = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, result);
            result = await _loanRepository.UpdateAsync(result);
            return ObjectMapper.Map<Loan, LoansDto>(result);
        }
    }
}
