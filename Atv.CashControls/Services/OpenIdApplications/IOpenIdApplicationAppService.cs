﻿using Volo.Abp.Application.Services;

namespace Atv.CashControls.Services.OpenIdApplications
{
    public interface IOpenIdApplicationAppService : IApplicationService
    {
        Task CreateAsync(CreateApplicationInput input);

    }
}
