﻿using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Dtos.Wallets;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.BankingNotes;
using Atv.CashControls.Repositories.Wallets;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.ObjectMapping;

namespace Atv.CashControls.Services.Wallets
{
    public class WalletService : ApplicationService, IWalletService
    {
        private readonly IWalletRepository _walletRepository;
        private readonly IBankingNoteRepository _bankingNoteRepository;

        public WalletService(IWalletRepository walletRepository, IBankingNoteRepository bankingNoteRepository)
        {
            _walletRepository = walletRepository;
            _bankingNoteRepository = bankingNoteRepository;
        }

        public async Task<decimal> ChangeMoney(Guid id, decimal amount, string method)
        {
            var isExist = await _walletRepository.GetAsync(id);

            if (isExist != null)
            {
                return await _walletRepository.ChangeMoney(id, amount, method);
            }

            return 0;
        }

        public async Task<WalletDto> CreateAsync(CreateWalletDto input)
        {
            var entity = ObjectMapper.Map<CreateWalletDto, Wallet>(input);
            entity = await _walletRepository.InsertAsync(entity);
            return ObjectMapper.Map<Wallet, WalletDto>(entity);
        }

        public Task DeleteAsync(Guid id)
        {
            return _walletRepository.DeleteAsync(id);
        }

        public async Task<WalletDto> GetAsync(Guid id)
        {
            var res = await _walletRepository.GetAsync(id);

            return ObjectMapper.Map<Wallet, WalletDto>(res);
        }

        public async Task<WalletDto> GetByIdAsync(Guid id)
        {
            return ObjectMapper.Map<Wallet, WalletDto>(await _walletRepository.GetByIdAsync(id));
        }

        public async Task<long> GetCount()
        {
            return await _walletRepository.GetCountAsync();
        }

        public async Task<PagedResultDto<WalletDto>> GetListAync(GetWalletInput input)
        {
            var totalCount = await _walletRepository.GetCountAsync(input.FilterText);

            var items = await _walletRepository.GetAllAsync(input.FilterText, input.Sorting, input.MaxResultCount, input.SkipCount);

            var bankings = await _bankingNoteRepository.GetListAsync(input.FilterText, input.Sorting, input.MaxResultCount, input.SkipCount);

            var walletDto = ObjectMapper.Map<List<Wallet>, List<WalletDto>>(items.ToList());

            foreach (var b in bankings)
            {
                walletDto.Add(new WalletDto
                {
                    Id = b.Id,
                    Name = b.BankingName,
                    Money = b.Money,
                    BankingMapping = 1
                });
            }

            return new PagedResultDto<WalletDto>
            {
                TotalCount = totalCount,
                Items = walletDto
            };
        }

        public async Task<WalletDto> UpdateAsync(Guid id, UpdateWalletDto input)
        {
            var queryable = await _walletRepository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var result = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, result);
            result = await _walletRepository.UpdateAsync(result);
            return ObjectMapper.Map<Wallet, WalletDto>(result);
        }
    }
}
