﻿using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Dtos.Wallets;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Services.Wallets
{
    public interface IWalletService
    {
        Task<PagedResultDto<WalletDto>> GetListAync(GetWalletInput input);
        Task<WalletDto> GetAsync(Guid id);
        Task<WalletDto> GetByIdAsync(Guid id);
        Task<WalletDto> CreateAsync(CreateWalletDto input);
        Task<WalletDto> UpdateAsync(Guid id, UpdateWalletDto input);
        Task DeleteAsync(Guid id);
        Task<long> GetCount();
        Task<decimal> ChangeMoney(Guid id, decimal amount, string method);
    }
}
