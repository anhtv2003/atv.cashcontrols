﻿using Atv.CashControls.Dtos.Depts;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Services.Depts
{
    public interface IDeptService
    {
        Task<PagedResultDto<DeptsDto>> GetListAync(GetDeptsInput input);
        Task<DeptsDto> GetAsync(Guid id);
        Task<DeptsDto> CreateAsync(CreateDeptsDto input);
        Task<DeptsDto> UpdateAsync(Guid id, UpdateDeptsDto input);
        Task DeleteAsync(Guid id);
        Task<long> GetCount();
    }
}
