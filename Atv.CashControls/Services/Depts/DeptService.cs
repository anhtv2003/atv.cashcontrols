﻿using Atv.CashControls.Dtos.Depts;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.Depts;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Services.Depts
{
    public class DeptService : ApplicationService, IDeptService
    {
        private readonly IDeptRepository _deptRepository;

        public DeptService(IDeptRepository deptRepository)
        {
            _deptRepository = deptRepository;
        }

        public async Task<DeptsDto> CreateAsync(CreateDeptsDto input)
        {
            var res = ObjectMapper.Map<CreateDeptsDto, Dept>(input);
            res = await _deptRepository.InsertAsync(res);
            return ObjectMapper.Map<Dept, DeptsDto>(res);            
        }

        public async Task DeleteAsync(Guid id)
        {
            await _deptRepository.DeleteAsync(id);
        }

        public async Task<DeptsDto> GetAsync(Guid id)
        {
            var res = await _deptRepository.GetAsync(id);
            return ObjectMapper.Map<Dept, DeptsDto>(res);
        }

        public async Task<long> GetCount()
        {
            return await _deptRepository.GetCountAsync();
        }

        public async Task<PagedResultDto<DeptsDto>> GetListAync(GetDeptsInput input)
        {
            var count = await _deptRepository.GetCountAsync(input.FilterText, input.Lender, input.FromDate, input.ToDate);

            var items = await _deptRepository.GetListAsync(input.FilterText, input.Lender, input.FromDate, input.ToDate, input.Sorting, input.MaxResultCount, input.SkipCount, input.Status.Value);

            return new PagedResultDto<DeptsDto>
            {
                TotalCount = count,
                Items = ObjectMapper.Map<List<Dept>, List<DeptsDto>>(items.ToList())
            };
        }

        public async Task<DeptsDto> UpdateAsync(Guid id, UpdateDeptsDto input)
        {
            var queryable = await _deptRepository.GetQueryableAsync();
            var query = queryable.Where(x => x.Id == id);
            var result = await AsyncExecuter.FirstOrDefaultAsync(query);
            ObjectMapper.Map(input, result);
            result = await _deptRepository.UpdateAsync(result);
            return ObjectMapper.Map<Dept, DeptsDto>(result);
        }
    }
}
