﻿namespace Atv.CashControls.Dtos.Commons
{
    public class ResponseMessage
    {
        public ResponseMessage()
        {
        }

        public ResponseMessage(bool success = false, object data = null, bool error = false)
        {
            Success = success;
            Data = data;
            Error = error;
        }

        public bool Success { get; set; }
        public object Data { get; set; }
        public bool Error { get; set; }
    }
}
