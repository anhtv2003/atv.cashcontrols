﻿using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.BankingNotes
{
    public class GetBankingNoteInput : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }
    }
}
