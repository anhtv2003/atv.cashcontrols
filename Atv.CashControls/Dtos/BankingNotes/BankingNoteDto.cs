﻿using Atv.CashControls.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.BankingNotes
{
    public class BankingNoteDto : AuditedEntityDto<Guid>
    {
        [DisplayName("BankingName")]
        public string BankingName { get; set; }

        [DisplayName("BankingLogo")]
        public string? BankingLogo { get; set; }

        [DisplayName("Price")]
        public decimal Money { get; set; }
    }

    public class BankingNoteTransactionDto : AuditedEntityDto<Guid>
    {
        [DisplayName("Price")]
        public decimal Amount { get; set; }

        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DisplayName("TransactionType")]
        public TransactionTypeEnum TransactionType { get; set; }

        [DisplayName("Place")]
        public string? Place { get; set; } // Nơi giao dịch

        [DisplayName("BankingReceive")]
        public string? BankingReceive { get; set; }

        [DisplayName("Receiver")]
        public string? Receiver { get; set; }

        [DisplayName("Remitter")]
        public string? Remitter { get; set; }

        [DisplayName("Image")]
        public string? Image { get; set; }

        public Guid BankingNoteId { get; set; }
    }
}
