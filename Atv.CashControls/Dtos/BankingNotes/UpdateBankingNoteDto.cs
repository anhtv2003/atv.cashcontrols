﻿using Atv.CashControls.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Atv.CashControls.Dtos.BankingNotes
{
    public class UpdateBankingNoteDto
    {
        [DisplayName("BankingName")]
        public string BankingName { get; set; }

        [DisplayName("BankingLogo")]
        public string? BankingLogo { get; set; }

        [DisplayName("Price")]
        [DisplayFormat(DataFormatString = "0:{N0}")]
        public string Money { get; set; }
    }

    public class UpdateBankingNoteTransactionDto
    {
        [DisplayName("Price")]
        [Required]
        public string Amount { get; set; }

        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DisplayName("TransactionType")]
        public TransactionTypeEnum TransactionType { get; set; }

        [DisplayName("Place")]
        public string? Place { get; set; } // Nơi giao dịch

        [DisplayName("BankingReceive")]
        public string? BankingReceive { get; set; }

        [DisplayName("Receiver")]
        public string? Receiver { get; set; }

        [DisplayName("Remitter")]
        public string? Remitter { get; set; }

        [DisplayName("Image")]
        public string? Image { get; set; }

        public Guid BankingNoteId { get; set; }
    }
}
