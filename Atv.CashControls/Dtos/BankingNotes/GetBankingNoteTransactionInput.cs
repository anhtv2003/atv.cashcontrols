using AutoFilterer.Attributes;
using AutoFilterer.Types;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.BankingNotes
{
    [Serializable]
    public class GetBankingNoteTransactionInput : FilterBase, IPagedAndSortedResultRequest
    {
        [CompareTo(
            nameof(BankingNoteTransactionDto.Place),
            nameof(BankingNoteTransactionDto.BankingReceive),
            nameof(BankingNoteTransactionDto.Receiver)
            )]
        [StringFilterOptions(AutoFilterer.Enums.StringFilterOption.Contains)]
        public string? Filter { get; set; }

        public Guid? NoteId { get; set; }

        // public Range<decimal>? Amount { get; set; }

        // IPagedAndSortedResultRequest implementation below.
        public int SkipCount { get; set; }

        public int MaxResultCount { get; set; }

        public string? Sorting { get; set; }

        public string? FilterText { get; private set; }
    }
}