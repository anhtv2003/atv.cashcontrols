﻿namespace Atv.CashControls.Dtos.CashCategories
{
    public class UpdateCashCategoryDto
    {
        public string Name { get; set; }

        public string? Description { get; set; }

        public int Status { get; set; }
    }
}
