﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Atv.CashControls.Dtos.CashCategories
{
    public class CreateCashCategoryDto
    {
        [Required]
        public string Name { get; set; }

        [TextArea]
        public string? Description { get; set; }

        [Required]
        public int Status { get; set; }
    }
}
