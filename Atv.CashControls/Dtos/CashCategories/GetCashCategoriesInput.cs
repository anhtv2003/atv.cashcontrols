﻿using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.CashCategories
{
    public class GetCashCategoriesInput : PagedAndSortedResultRequestDto
    {
        public GetCashCategoriesInput()
        {
        }

        public GetCashCategoriesInput(string? filter = null, string? name = null)
        {
            FilterText = filter ?? string.Empty;
            Name = name ?? string.Empty;
        }

        public string? FilterText { get; set; }
        public string? Name { get; set; }
    }
}
