﻿using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.CashCategories
{
    public class CashCategoryDto : AuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public string? Description { get; set; }

        public int Status { get; set; }
    }
}
