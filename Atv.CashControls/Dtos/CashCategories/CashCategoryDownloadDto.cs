﻿namespace Atv.CashControls.Dtos.CashCategories
{
    public class CashCategoryDownloadDto
    {
        public string DownloadToken { get; set; }
        public string FileType { get; set; }
        public string FilterText { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
    }
}
