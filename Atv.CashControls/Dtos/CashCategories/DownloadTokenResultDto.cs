﻿namespace Atv.CashControls.Dtos.CashCategories
{
    public class DownloadTokenResultDto
    {
        public string Token { get; set; }
    }
}
