﻿using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Incomes
{
    public class GetIncomeInput : PagedAndSortedResultRequestDto
    {
        public GetIncomeInput()
        {
        }

        public GetIncomeInput(string? filterText)
        {
            FilterText = filterText;
        }

        public string? FilterText { get; set; }
    }
}
