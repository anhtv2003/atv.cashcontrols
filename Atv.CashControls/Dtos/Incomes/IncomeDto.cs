﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Incomes
{
    public class IncomeDto : AuditedEntityDto<Guid>
    {
        [Required]
        public DateTime Date { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public IncomeTypeEnum Type { get; set; } //lương

        public string? Source { get; set; } // công ty...

        public string? Description { get; set; }
    }
}
