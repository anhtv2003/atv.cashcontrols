﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Atv.CashControls.Dtos.Incomes
{
    public class CreateIncomeDto
    {
        [Required]
        public DateTime Date { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public IncomeTypeEnum Type { get; set; } //lương

        public string? Source { get; set; } // công ty...

        [TextArea]
        public string? Description { get; set; }
    }
}
