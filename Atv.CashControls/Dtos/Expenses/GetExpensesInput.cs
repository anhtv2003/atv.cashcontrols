﻿using AutoFilterer.Attributes;
using AutoFilterer.Types;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Expenses
{
    [Serializable]
    public class GetExpensesInput : FilterBase, IPagedAndSortedResultRequest
    {

        [CompareTo(nameof(ExpenseDto.Name))]
        [StringFilterOptions(AutoFilterer.Enums.StringFilterOption.Contains)]
        public string? Filter { get; set; }

        public Guid? CategoryID { get; set; }

        public Range<DateTime>? Date { get; set; }

        public int? Status { get; set; }

        // IPagedAndSortedResultRequest implementation below.
        public int SkipCount { get; set; }

        public int MaxResultCount { get; set; }

        public string? Sorting { get; set; }

        public string? FilterText { get; internal set; }
    }
}
