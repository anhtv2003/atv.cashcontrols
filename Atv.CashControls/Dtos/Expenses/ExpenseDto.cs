﻿using Atv.CashControls.Enums;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Expenses
{
    public class ExpenseDto : AuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public DateTime Date { get; set; }

        public string Month { get; set; }

        public Guid? CategoryID { get; set; }

        public string? CategoryName { get; set; }

        public string? Note { get; set; }

        public Status Status { get; set; }
    }
}
