﻿using Atv.CashControls.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Atv.CashControls.Dtos.Expenses
{
    public class CreateExpenseDto
    {
        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Price")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public string Price { get; set; }

        [DisplayName("Quantity")]
        public int Quantity { get; set; } = 1;

        [Required]
        [DisplayName("Date")]
        public DateTime Date { get; set; } = DateTime.Now;

        [DisplayName("Category")]
        public Guid? CategoryID { get; set; }

        [TextArea]
        [DisplayName("Note")]
        public string? Note { get; set; }

        public Guid? WalletId { get; set; }

        [Required]
        [DisplayName("Status")]
        public Status Status { get; set; }
    }
}
