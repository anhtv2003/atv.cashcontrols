﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Atv.CashControls.Dtos.Wallets
{
    public class UpdateWalletDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public string Money { get; set; }

        [TextArea]
        public string? Description { get; set; }
    }
}
