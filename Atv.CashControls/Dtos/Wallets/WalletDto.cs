﻿using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Wallets
{
    public class WalletDto : AuditedEntityDto<Guid>
    {
        public string Name { get; set; }

        public decimal Money { get; set; }

        public string? Description { get; set; }

        public int BankingMapping { get; set; } = 0;
    }
}
