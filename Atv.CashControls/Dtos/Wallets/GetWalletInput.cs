﻿using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Wallets
{
    public class GetWalletInput : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }
    }
}
