﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public class DayExpenseStatisticWidgetResultDto
    {
        public Dictionary<string, decimal> Data { get; set; }
    }

    
}
