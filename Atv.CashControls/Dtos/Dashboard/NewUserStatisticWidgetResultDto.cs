﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public class NewUserStatisticWidgetResultDto
    {
        public Dictionary<string, int> Data { get; set; }
    }
}
