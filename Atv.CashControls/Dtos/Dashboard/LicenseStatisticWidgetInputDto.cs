﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public class LicenseStatisticWidgetInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
