﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public enum NewUserStatisticFrequency
    {
        Daily,
        Monthly
    }
}
