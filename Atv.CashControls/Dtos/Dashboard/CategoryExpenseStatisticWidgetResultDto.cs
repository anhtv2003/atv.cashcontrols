﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public class CategoryExpenseStatisticWidgetResultDto
    {
        public Dictionary<string, decimal> Data { get; set; }
    }

    public class CategoryExpenseStatisticData
    {
        public decimal TotalAmount { get; set; }
        public float Percent { get; set; }
    }
}
