﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public class CategoryExpenseStatisticWidgetInputDto
    {
        public int Month { get; set; }

        public int Year { get; set; }
    }
}
