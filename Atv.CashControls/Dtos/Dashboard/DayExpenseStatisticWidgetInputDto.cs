﻿using System.ComponentModel.DataAnnotations;

namespace Atv.CashControls.Dtos.Dashboard
{
    public class DayExpenseStatisticWidgetInputDto
    {
        [DataType(DataType.Date)]
        public int Month { get; set; }

        [DataType(DataType.Date)]
        public int Year { get; set; }
    }
}
