﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public class LicenseStatistiWidgetResultDto
    {
        public Dictionary<string, int> Data { get; set; }
    }
}
