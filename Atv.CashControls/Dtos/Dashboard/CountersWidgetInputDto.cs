﻿namespace Atv.CashControls.Dtos.Dashboard
{
    public class CountersWidgetInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
