﻿using Atv.CashControls.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Atv.CashControls.Dtos.Loans
{
    public class CreateLoansDto
    {
        [Required]
        [DisplayName("Amount")]
        [DisplayFormat(DataFormatString = "0:{N0}")]
        //[RegularExpression(@"^\d+$", ErrorMessage = "Amount must be numeric.")]
        public string Amount { get; set; } // Số tiền cho vay

        [Required]
        [DisplayName("Borrower")]
        public string Borrower { get; set; } // Người vay

        [Required]
        [DisplayName("Date")]
        public DateTime Date { get; set; } = DateTime.Now; // Ngày cho vay

        [DisplayName("DueDate")]
        public DateTime? DueDate { get; set; } = DateTime.Now;  // Ngày người vay phải trả.

        [DisplayName("Description")]
        [TextArea]
        public string? Description { get; set; }

        [DisplayName("Status")]
        public DeptStatusEnum Status { get; set; }
    }
}
