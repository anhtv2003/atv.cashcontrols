﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Atv.CashControls.Dtos.Loans
{
    public class UpdateLoansDto
    {
        [Required]
        [DisplayName("Amount")]
        [DisplayFormat(DataFormatString = "0:{N0}")]
        public string Amount { get; set; } // Số tiền cho vay

        [Required]
        [DisplayName("Borrower")]
        public string Borrower { get; set; } // Người vay

        [Required]
        [DisplayName("Date")]
        public DateTime Date { get; set; } // Ngày cho vay

        [DisplayName("DueDate")]
        public DateTime? DueDate { get; set; } // Ngày người vay phải trả.

        [DisplayName("Description")]
        public string? Description { get; set; }

        [DisplayName("Status")]
        public DeptStatusEnum Status { get; set; }
    }
}
