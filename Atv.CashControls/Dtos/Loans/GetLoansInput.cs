﻿using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Loans
{
    public class GetLoansInput : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Status { get; set; } = -1;
    }
}
