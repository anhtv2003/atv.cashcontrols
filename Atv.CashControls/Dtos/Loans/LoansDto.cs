﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Loans
{
    public class LoansDto : AuditedEntityDto<Guid>
    {
        public decimal Amount { get; set; } // Số tiền cho vay

        public string Borrower { get; set; } // Người vay

        public DateTime Date { get; set; } // Ngày cho vay

        public DateTime? DueDate { get; set; } // Ngày người vay phải trả.

        public string? Description { get; set; }

        public DeptStatusEnum Status { get; set; }
    }
}
