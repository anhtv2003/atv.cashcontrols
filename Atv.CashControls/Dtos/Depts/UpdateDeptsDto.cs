﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;

namespace Atv.CashControls.Dtos.Depts
{
    public class UpdateDeptsDto
    {
        [Required]
        public decimal Price { get; set; }

        [Required]
        public string? Lender { get; set; } // Người cho vay

        [Required]
        public DateTime Date { get; set; } // Ngày vay

        public DateTime DueDate { get; set; } // Ngày đến hạn

        public DateTime? ActualPaymentDate { get; set; } // Ngày trả thực tế

        public string? Description { get; set; }

        [Required]
        public DeptStatusEnum Status { get; set; }
    }
}
