﻿using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Depts
{
    public class GetDeptsInput : PagedAndSortedResultRequestDto
    {
        public string? FilterText { get; set; }
        public string? Lender { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Status { get; set; } = -1;
    }
}
