﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Depts
{
    public class DeptsDto : AuditedEntityDto<Guid>
    {
        public string? UserDisplayName { get; set; }

        public decimal Price { get; set; }

        public string? Lender { get; set; } // Người cho vay

        public DateTime Date { get; set; } // Ngày vay

        public DateTime DueDate { get; set; } // Ngày đến hạn

        public DateTime? ActualPaymentDate { get; set; } // Ngày trả thực tế

        public string? Description { get; set; }

        public DeptStatusEnum Status { get; set; }
    }
}
