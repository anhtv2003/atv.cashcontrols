﻿using Atv.CashControls.Enums;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;

namespace Atv.CashControls.Dtos.Depts
{
    public class CreateDeptsDto
    {
        [Required]
        public decimal Price { get; set; }

        [Required]
        public string? Lender { get; set; } // Người cho vay

        [Required]
        public DateTime Date { get; set; } = DateTime.Now; // Ngày vay

        public DateTime DueDate { get; set; } = DateTime.Now; // Ngày đến hạn

        public DateTime? ActualPaymentDate { get; set; } // Ngày trả thực tế

        [TextArea]
        public string? Description { get; set; }

        [Required]
        public DeptStatusEnum Status { get; set; }
    }
}
