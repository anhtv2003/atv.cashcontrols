using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFilterer.Attributes;
using AutoFilterer.Types;
using Volo.Abp.Application.Dtos;

namespace Atv.CashControls.Dtos.Books
{
    [Serializable]
    // We'll leave Paging and Sorting to ABP, we'll use only filtering feature of AutoFilterer.
    // So using FilterBase as a base class is enough.
    public class BookGetListInput : FilterBase, IPagedAndSortedResultRequest
    {
        [CompareTo(
                nameof(BookDto.Title),
                nameof(BookDto.Language),
                nameof(BookDto.Author),
                nameof(BookDto.Country)
                )]
        [StringFilterOptions(AutoFilterer.Enums.StringFilterOption.Contains)]
        public string? Filter { get; set; }

        public Range<int> TotalPage { get; set; }

        public Range<int> Year { get; set; }

        // IPagedAndSortedResultRequest implementation below.
        public int SkipCount { get; set; }

        public int MaxResultCount { get; set; }

        public string? Sorting { get; set; }

        public string? FilterText { get; internal set; }
    }
}