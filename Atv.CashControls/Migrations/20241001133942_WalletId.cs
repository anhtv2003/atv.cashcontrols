﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Atv.CashControls.Migrations
{
    /// <inheritdoc />
    public partial class WalletId : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "WalletId",
                table: "Expenses",
                type: "uuid",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WalletId",
                table: "Expenses");
        }
    }
}
