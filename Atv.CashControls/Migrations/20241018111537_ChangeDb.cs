﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Atv.CashControls.Migrations
{
    /// <inheritdoc />
    public partial class ChangeDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankingNoteTransactions_AbpUsers_UserId",
                table: "BankingNoteTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_CashCategories_AbpUsers_UserId",
                table: "CashCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Depts_AbpUsers_UserId",
                table: "Depts");

            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_AbpUsers_UserId",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Loans_AbpUsers_UserId",
                table: "Loans");

            migrationBuilder.DropIndex(
                name: "IX_Loans_UserId",
                table: "Loans");

            migrationBuilder.DropIndex(
                name: "IX_Expenses_UserId",
                table: "Expenses");

            migrationBuilder.DropIndex(
                name: "IX_Depts_UserId",
                table: "Depts");

            migrationBuilder.DropIndex(
                name: "IX_CashCategories_UserId",
                table: "CashCategories");

            migrationBuilder.DropIndex(
                name: "IX_BankingNoteTransactions_UserId",
                table: "BankingNoteTransactions");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Loans");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Expenses");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Depts");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "CashCategories");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "BankingNoteTransactions");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Loans",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Expenses",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Depts",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "CashCategories",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "BankingNoteTransactions",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Loans_UserId",
                table: "Loans",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Expenses_UserId",
                table: "Expenses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Depts_UserId",
                table: "Depts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CashCategories_UserId",
                table: "CashCategories",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_BankingNoteTransactions_UserId",
                table: "BankingNoteTransactions",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankingNoteTransactions_AbpUsers_UserId",
                table: "BankingNoteTransactions",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CashCategories_AbpUsers_UserId",
                table: "CashCategories",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Depts_AbpUsers_UserId",
                table: "Depts",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_AbpUsers_UserId",
                table: "Expenses",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Loans_AbpUsers_UserId",
                table: "Loans",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
