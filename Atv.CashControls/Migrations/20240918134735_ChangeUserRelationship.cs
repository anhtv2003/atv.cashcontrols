﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Atv.CashControls.Migrations
{
    /// <inheritdoc />
    public partial class ChangeUserRelationship : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankingNoteTransactions_BankingNotes_BankingNoteId",
                table: "BankingNoteTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_BankingNoteTransactions_IdentityUser_UserId",
                table: "BankingNoteTransactions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BankingNoteTransactions",
                table: "BankingNoteTransactions");

            migrationBuilder.RenameTable(
                name: "BankingNoteTransactions",
                newName: "BankingNoteTransaction");

            migrationBuilder.RenameIndex(
                name: "IX_BankingNoteTransactions_UserId",
                table: "BankingNoteTransaction",
                newName: "IX_BankingNoteTransaction_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_BankingNoteTransactions_BankingNoteId",
                table: "BankingNoteTransaction",
                newName: "IX_BankingNoteTransaction_BankingNoteId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BankingNoteTransaction",
                table: "BankingNoteTransaction",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BankingNoteTransaction_BankingNotes_BankingNoteId",
                table: "BankingNoteTransaction",
                column: "BankingNoteId",
                principalTable: "BankingNotes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankingNoteTransaction_IdentityUser_UserId",
                table: "BankingNoteTransaction",
                column: "UserId",
                principalTable: "IdentityUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankingNoteTransaction_BankingNotes_BankingNoteId",
                table: "BankingNoteTransaction");

            migrationBuilder.DropForeignKey(
                name: "FK_BankingNoteTransaction_IdentityUser_UserId",
                table: "BankingNoteTransaction");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BankingNoteTransaction",
                table: "BankingNoteTransaction");

            migrationBuilder.RenameTable(
                name: "BankingNoteTransaction",
                newName: "BankingNoteTransactions");

            migrationBuilder.RenameIndex(
                name: "IX_BankingNoteTransaction_UserId",
                table: "BankingNoteTransactions",
                newName: "IX_BankingNoteTransactions_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_BankingNoteTransaction_BankingNoteId",
                table: "BankingNoteTransactions",
                newName: "IX_BankingNoteTransactions_BankingNoteId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BankingNoteTransactions",
                table: "BankingNoteTransactions",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BankingNoteTransactions_BankingNotes_BankingNoteId",
                table: "BankingNoteTransactions",
                column: "BankingNoteId",
                principalTable: "BankingNotes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BankingNoteTransactions_IdentityUser_UserId",
                table: "BankingNoteTransactions",
                column: "UserId",
                principalTable: "IdentityUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
