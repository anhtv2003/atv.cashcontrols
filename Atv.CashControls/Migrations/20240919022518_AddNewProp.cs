﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Atv.CashControls.Migrations
{
    /// <inheritdoc />
    public partial class AddNewProp : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Remitter",
                table: "BankingNoteTransactions",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remitter",
                table: "BankingNoteTransactions");
        }
    }
}
