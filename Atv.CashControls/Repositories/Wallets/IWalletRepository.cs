﻿using Atv.CashControls.Entities;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.Wallets
{
    public interface IWalletRepository : IRepository<Wallet, Guid>
    {
        Task<decimal> ChangeMoney(Guid id, decimal amount, string method, CancellationToken cancellationToken = default);

        Task<long> GetCountAsync(
            string filterText = null,
            CancellationToken cancellationToken = default
            );

        Task<List<Wallet>> GetAllAsync(
            string? filterText = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string[] includes = null,
            CancellationToken cancellationToken = default
            );

        Task<Wallet> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);

    }
}
