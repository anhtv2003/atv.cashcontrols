﻿using Atv.CashControls.Data;
using Atv.CashControls.Entities;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.Wallets
{
    public class EfCoreWalletRepository : EfCoreRepository<CashControlsDbContext, Wallet, Guid>, IWalletRepository
    {
        private readonly CashControlsDbContext _dbContext;

        public EfCoreWalletRepository(CashControlsDbContext dbContext, IDbContextProvider<CashControlsDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }

        protected virtual IQueryable<Wallet> ApplyFilter(IQueryable<Wallet> query, string filterText = null)
        {
            return query
                .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.Name.Contains(filterText));
        }

        public async Task<List<Wallet>> GetAllAsync(string? filterText = null, string sorting = null, int maxResultCount = int.MaxValue, int skipCount = 0, string[] includes = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText);

            if (includes != null)
            {
                query = query.Include(includes.First());

                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
            }

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        public async Task<long> GetCountAsync(string filterText = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<decimal> ChangeMoney(Guid id, decimal amount, string method, CancellationToken cancellationToken = default)
        {
            if (amount > 0)
            {
                var entity = (await GetDbSetAsync()).Where(x => x.Id == id).FirstOrDefault();

                if (entity != null)
                {
                    switch (method)
                    {
                        case "sub":
                            entity.Money -= amount;
                            break;
                        case "add":
                            entity.Money += amount;
                            break;

                    }                    

                    return entity.Money;
                }
            }

            return 0;
        }

        public async Task<Wallet> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.Wallets.Where(x => x.Id == id).FirstOrDefaultAsync();
        }
    }
}
