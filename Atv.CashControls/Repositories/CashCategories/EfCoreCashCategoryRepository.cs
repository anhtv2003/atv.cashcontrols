﻿using Atv.CashControls.Data;
using Atv.CashControls.Entities;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.CashCategories
{
    public class EfCoreCashCategoryRepository : EfCoreRepository<CashControlsDbContext, CashCategory, Guid>, ICashCategoryRepository
    {
        public EfCoreCashCategoryRepository(IDbContextProvider<CashControlsDbContext> dbContextProvider) : base (dbContextProvider)
        {
            
        }

        public async Task<long> GetCountAsync(string filterText = null, string name = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText, name);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<List<CashCategory>> GetListAsync(string filterText = null, string name = null, string sorting = null, int maxResultCount = int.MaxValue, int skipCount = 0, string[] includes = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText, name);

            if (includes != null)
            {
                query = query.Include(includes.First());

                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
            }

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        public async Task<List<CashCategory>> GetList(CancellationToken cancellationToken = default)
        {
            var res = (await GetQueryableAsync()).OrderBy(x => x.Name).ToList();

            return res;
        }

        protected virtual IQueryable<CashCategory> ApplyFilter (IQueryable<CashCategory> query, string filterText = null, string name = null)
        {
            return query
                .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.Name.Contains(filterText))
                .WhereIf(!string.IsNullOrWhiteSpace(name), e => e.Name.Contains(name));
        }
    }
}
