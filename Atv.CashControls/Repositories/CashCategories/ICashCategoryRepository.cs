﻿using Atv.CashControls.Entities;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.CashCategories
{
    public interface ICashCategoryRepository : IRepository<CashCategory, Guid>
    {
        Task<List<CashCategory>> GetListAsync(
            string filterText = null,
            string name = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0, 
            string[] includes = null,
            CancellationToken cancellationToken = default
        );

        Task<List<CashCategory>> GetList(CancellationToken cancellationToken = default);

        Task<long> GetCountAsync(
            string filterText = null,
            string name = null,
            CancellationToken cancellationToken = default
        );
    }
}
