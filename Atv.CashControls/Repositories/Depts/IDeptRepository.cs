﻿using Atv.CashControls.Entities;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.Depts
{
    public interface IDeptRepository : IRepository<Dept, Guid>
    {
        Task<IEnumerable<Dept>> GetListAsync(
            string filterText = null,
            string lender = null,
            DateTime? fromDate = null,
            DateTime? toDate = null,
            string sorting = null,
            int maxResultCount = 10,
            int skipCount = 0,
            int status = -1,
            string[] includes = null,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string filterText = null,
            string lender = null,
            DateTime? fromDate = null,
            DateTime? toDate = null,
            string sorting = null,
            int maxResultCount = 10,
            int skipCount = 0,
            int status = -1,
            CancellationToken cancellationToken = default
        );
    }
}
