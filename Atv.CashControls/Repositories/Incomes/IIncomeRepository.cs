﻿using Atv.CashControls.Entities;
using AutoFilterer.Types;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.Incomes
{
    public interface IIncomeRepository : IRepository<Income, Guid>
    {
        Task<List<Income>> GetListAsync(
            string filterText = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string[] includes = null,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string filterText = null,
            CancellationToken cancellationToken = default
        );
    }
}
