﻿using Atv.CashControls.Data;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.CashCategories;
using AutoFilterer.Types;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.Incomes
{
    public class EFCoreIncomeRepository : EfCoreRepository<CashControlsDbContext, Income, Guid>, IIncomeRepository
    {
        public EFCoreIncomeRepository(IDbContextProvider<CashControlsDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<long> GetCountAsync(string filterText = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<List<Income>> GetListAsync(string filterText = null, string sorting = null, int maxResultCount = int.MaxValue, int skipCount = 0, string[] includes = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText);

            if (includes != null)
            {
                query = query.Include(includes.First());

                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
            }

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        protected virtual IQueryable<Income> ApplyFilter(IQueryable<Income> query, string filterText = null)
        {
            return query;
        }
    }
}
