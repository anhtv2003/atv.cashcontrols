﻿using Atv.CashControls.Dtos.Dashboard;

namespace Atv.CashControls.Repositories.Dashboard
{
    public interface IDashboardRepository
    {
        Task<DayExpenseStatisticWidgetResultDto> GetDayExpenseStatisticWidgetAsync(DayExpenseStatisticWidgetInputDto input);

        Task<CategoryExpenseStatisticWidgetResultDto> CategoryExpenseStatisticWidgetAsync(CategoryExpenseStatisticWidgetInputDto input);
    }
}
