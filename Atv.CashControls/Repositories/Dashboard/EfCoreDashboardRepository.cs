﻿using Atv.CashControls.Data;
using Atv.CashControls.Dtos.Dashboard;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.Dashboard
{
    public class EfCoreDashboardRepository : IDashboardRepository
    {
        private readonly CashControlsDbContext _dbContext;

        public EfCoreDashboardRepository(CashControlsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<CategoryExpenseStatisticWidgetResultDto> CategoryExpenseStatisticWidgetAsync(CategoryExpenseStatisticWidgetInputDto input)
        {
            var result = new CategoryExpenseStatisticWidgetResultDto()
            {
                Data = new Dictionary<string, decimal>()
            };

            var query = _dbContext.Expenses.Include("CashCategory").Select(x => x);

            var total = query.Sum(x => x.Price);

            var expenses = await query.Where(x => x.Date.Month == input.Month && x.Date.Year == input.Year)
                .GroupBy(x => x.CashCategory.Name)
                .Select(g => new
                {
                    Category = g.Key,
                    TotalAmount = g.Sum(x => x.Price),
                    Percent = (float) (g.Sum(x => x.Price) / total) * 100
                }).ToListAsync();

            foreach (var exp in expenses)
            {
                result.Data.Add(exp.Category, exp.TotalAmount);
            }

            return result;
        }

        public async Task<DayExpenseStatisticWidgetResultDto> GetDayExpenseStatisticWidgetAsync(DayExpenseStatisticWidgetInputDto input)
        {
            var result = new DayExpenseStatisticWidgetResultDto()
            {
                Data = new Dictionary<string, decimal>()
            };

            var query = _dbContext.Expenses.Select(x => x);
            var expenses = await query.Where(x => x.Date.Month == input.Month && x.Date.Year == input.Year)
                .GroupBy(x => x.Date)
                .Select(g => new
                {
                    Date = g.Key,
                    TotalAmount = g.Sum(x => x.Price)
                })
                .ToListAsync();


            foreach (var exp in expenses)
            {
                result.Data.Add(exp.Date.ToString("dd-MM-yyyy"), exp.TotalAmount);
            }

            return result;
        }
    }
}
