﻿using Atv.CashControls.Data;
using Atv.CashControls.Entities;
using Atv.CashControls.Enums;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.Loans
{
    public class EfCoreLoanRepository : EfCoreRepository<CashControlsDbContext, Loan, Guid>, ILoanRepository
    {
        public EfCoreLoanRepository(IDbContextProvider<CashControlsDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<long> GetCountAsync(string filterText = null, DateTime? fromDate = null, DateTime? toDate = null, string sorting = null, int maxResultCount = 10, int skipCount = 0, int status = -1, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter(await GetDbSetAsync(), filterText, fromDate, toDate, status);

            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<IEnumerable<Loan>> GetListAsync(string filterText = null, DateTime? fromDate = null, DateTime? toDate = null, string sorting = null, int maxResultCount = 10, int skipCount = 0, int status = -1, string[] includes = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter(await GetDbSetAsync(), filterText, fromDate, toDate, status);

            if (includes != null)
            {
                query = query.Include(includes.First());

                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
            }

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        protected virtual IQueryable<Loan> ApplyFilter(
            IQueryable<Loan> query,
            string filterText = null,
            DateTime? fromDate = null,
            DateTime? toDate = null,
            int? status = -1)
        {
            return query
                .WhereIf(!string.IsNullOrEmpty(filterText), x => x.Borrower.Contains(filterText))
                .WhereIf(fromDate.HasValue, x => x.Date >= fromDate)
                .WhereIf(toDate.HasValue, x => x.Date <= toDate)
                .WhereIf(status == 1, x => x.Status == DeptStatusEnum.DaThanhToan)
                .WhereIf(status == 0, x => x.Status == DeptStatusEnum.ChuaThanhToan);
        }
    }
}
