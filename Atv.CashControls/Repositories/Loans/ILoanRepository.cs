﻿using Atv.CashControls.Entities;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.Loans
{
    public interface ILoanRepository : IRepository<Loan, Guid>
    {
        Task<IEnumerable<Loan>> GetListAsync(
            string filterText = null,
            DateTime? fromDate = null,
            DateTime? toDate = null,
            string sorting = null,
            int maxResultCount = 10,
            int skipCount = 0,
            int status = -1,
            string[] includes = null,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string filterText = null,
            DateTime? fromDate = null,
            DateTime? toDate = null,
            string sorting = null,
            int maxResultCount = 10,
            int skipCount = 0,
            int status = -1,
            CancellationToken cancellationToken = default
        );
    }
}
