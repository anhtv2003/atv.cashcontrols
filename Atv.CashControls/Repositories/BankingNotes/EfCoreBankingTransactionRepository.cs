using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Atv.CashControls.Data;
using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Entities;
using AutoFilterer.Extensions;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.BankingNotes
{
    public class EfCoreBankingTransactionRepository : EfCoreRepository<CashControlsDbContext, BankingNoteTransaction, Guid>, IBankingTransactionRepository
    {
        private readonly CashControlsDbContext _dbContext;

        public EfCoreBankingTransactionRepository(CashControlsDbContext dbContext, IDbContextProvider<CashControlsDbContext> dbContextProvider) 
            : base(dbContextProvider)
        {
            _dbContext = dbContext;
        }

        public async Task<BankingNoteTransaction> GetByIdAsync(Guid id, Guid bankingId, CancellationToken cancellationToken = default)
        {
            return await _dbContext.BankingNoteTransactions
                .Where(x => x.Id == id && x.BankingNoteId == bankingId)
                .FirstOrDefaultAsync();
        }

        public async Task<BankingNoteTransaction> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return await _dbContext.BankingNoteTransactions
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<long> GetCountAsync(string filterText = null, string sorting = null, CancellationToken cancellationToken = default)
        {
            var input = new GetBankingNoteTransactionInput
            {
                Filter = filterText,
                Sorting = sorting,
            };

            var query = await CreateFilteredQueryAsync((await GetDbSetAsync()), input);

            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<IEnumerable<BankingNoteTransaction>> GetListAsync(Guid? noteId = null, string filterText = null, decimal? amount = null, string sorting = null, int maxResultCount = int.MaxValue, int skipCount = 0, string[] includes = null, CancellationToken cancellationToken = default)
        {
            var input = new GetBankingNoteTransactionInput
            {
                Filter = filterText,
                Sorting = sorting,
                MaxResultCount = maxResultCount,
                SkipCount = skipCount,
            };

            var query = await CreateFilteredQueryAsync((await GetQueryableAsync()).Where(x => x.BankingNoteId == noteId).OrderByDescending(x => x.Date), input);

            if (includes != null)
            {
                query = query.Include(includes.First());

                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
            }

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        protected async Task<IQueryable<BankingNoteTransaction>> CreateFilteredQueryAsync(IQueryable<BankingNoteTransaction> query, GetBankingNoteTransactionInput input)
        {
            // return query
            //     .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.BankingReceive.Contains(input.Filter));

            return query.ApplyFilter(input);
        }
    }
}