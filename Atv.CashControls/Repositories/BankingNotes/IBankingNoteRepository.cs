﻿using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Entities;
using Atv.CashControls.Enums;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.BankingNotes
{
    public interface IBankingNoteRepository : IRepository<BankingNote, Guid>
    {
        Task<decimal> GetMoneyOfBanking(Guid bankingId);

        Task UpdateMoneyWhenCreateTransaction(Guid bankingNoteId, decimal price, string method);

        Task<IEnumerable<BankingNote>> GetListAsync(
            string filterText = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string[] includes = null,
            CancellationToken cancellationToken = default
            );

        Task<long> GetCountAsync(
            string filterText = null,
            string sorting = null,
            CancellationToken cancellationToken = default
            );
    }
}
