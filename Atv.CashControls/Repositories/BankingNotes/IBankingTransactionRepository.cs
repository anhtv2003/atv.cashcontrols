using Atv.CashControls.Entities;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.BankingNotes
{
    public interface IBankingTransactionRepository : IRepository<BankingNoteTransaction, Guid>
    {
        Task<IEnumerable<BankingNoteTransaction>> GetListAsync(
            Guid? noteId = null,
            string filterText = null,
            decimal? amount = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string[] includes = null,
            CancellationToken cancellationToken = default
            );

        Task<long> GetCountAsync(
            string filterText = null,
            string sorting = null,
            CancellationToken cancellationToken = default
            );

        Task<BankingNoteTransaction> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);
        Task<BankingNoteTransaction> GetByIdAsync(Guid id, Guid bankingId, CancellationToken cancellationToken = default);
    }
}