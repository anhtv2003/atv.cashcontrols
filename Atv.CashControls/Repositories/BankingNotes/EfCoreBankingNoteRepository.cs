﻿using Atv.CashControls.Data;
using Atv.CashControls.Entities;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.BankingNotes
{
    public class EfCoreBankingNoteRepository 
        : EfCoreRepository<CashControlsDbContext, BankingNote, Guid>, IBankingNoteRepository
    {
        public EfCoreBankingNoteRepository(IDbContextProvider<CashControlsDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<long> GetCountAsync(string filterText = null, string sorting = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetDbSetAsync()), filterText);
            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<IEnumerable<BankingNote>> GetListAsync(string filterText = null, string sorting = null, int maxResultCount = int.MaxValue, int skipCount = 0, string[] includes = null, CancellationToken cancellationToken = default)
        {
            var query = ApplyFilter((await GetQueryableAsync()), filterText);

            if (includes != null)
            {
                query = query.Include(includes.First());

                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
            }

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        public async Task<decimal> GetMoneyOfBanking(Guid bankingId)
        {
            var banking = (await GetDbSetAsync()).Where(x => x.Id == bankingId).FirstOrDefault();

            return banking.Money;
        }

        public async Task UpdateMoneyWhenCreateTransaction(Guid bankingNoteId, decimal price, string method)
        {
            var banking = (await GetDbSetAsync()).Where(x => x.Id == bankingNoteId).FirstOrDefault();

            if (banking != null)
            {
                switch (method)
                {
                    case "sub":
                        banking.Money -= price;
                        break;
                    case "add":
                        banking.Money += price;
                        break;
                }
            }
        }

        protected virtual IQueryable<BankingNote> ApplyFilter(IQueryable<BankingNote> query, string filterText = null)
        {
            return query
                .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.BankingName.Contains(filterText));
        }
    }
}
