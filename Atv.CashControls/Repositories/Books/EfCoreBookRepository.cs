using Atv.CashControls.Data;
using Atv.CashControls.Dtos.Books;
using Atv.CashControls.Entities;
using AutoFilterer.Extensions;
using AutoFilterer.Types;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.Books
{
    public class EfCoreBookRepository : EfCoreRepository<CashControlsDbContext, Book, Guid>, IBookRepository
    {
        public EfCoreBookRepository(IDbContextProvider<CashControlsDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<long> GetCountAsync(string filterText = null, Range<int>? totalPage = null, Range<int>? year = null, string sorting = null, CancellationToken cancellationToken = default)
        {
            var GetInput = new BookGetListInput
            {
                Filter = filterText,
                TotalPage = totalPage,
                Year = year,
                Sorting = sorting
            };

            var query = await CreateFilteredQueryAsync(await GetQueryableAsync(), GetInput);

            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<IEnumerable<Book>> GetListAsync(string filterText = null, Range<int>? totalPage = null, Range<int>? year = null, string sorting = null, int maxResultCount = int.MaxValue, int skipCount = 0, string[] includes = null, CancellationToken cancellationToken = default)
        {
            var GetInput = new BookGetListInput
            {
                Filter = filterText,
                TotalPage = totalPage,
                Year = year,
                Sorting = sorting,
                MaxResultCount = maxResultCount,
                SkipCount = skipCount,
            };

            var query = await CreateFilteredQueryAsync(await GetQueryableAsync(), GetInput);

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        protected async Task<IQueryable<Book>> CreateFilteredQueryAsync(IQueryable<Book> query, BookGetListInput input)
        {
            return query.ApplyFilter(input);
        }
    }
}