using Atv.CashControls.Entities;
using AutoFilterer.Types;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.Books
{
    public interface IBookRepository : IRepository<Book, Guid>
    {
        Task<IEnumerable<Book>> GetListAsync(
            string filterText = null,
            Range<int>? totalPage = null,
            Range<int>? year = null,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string[] includes = null,
            CancellationToken cancellationToken = default
            );

        Task<long> GetCountAsync(
            string filterText = null,
            Range<int>? totalPage = null,
            Range<int>? year = null,
            string sorting = null,
            CancellationToken cancellationToken = default
            );
    }
}