﻿using Atv.CashControls.Data;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Entities;
using Atv.CashControls.Enums;
using Atv.CashControls.Repositories.BankingNotes;
using Atv.CashControls.Repositories.Wallets;
using AutoFilterer.Enums;
using AutoFilterer.Extensions;
using AutoFilterer.Types;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace Atv.CashControls.Repositories.Expenses
{
    public class EfCoreExpenseRepository : EfCoreRepository<CashControlsDbContext, Expense, Guid>, IExpenseRepository
    {
        private readonly IWalletRepository _walletRepository;
        private readonly IBankingNoteRepository _bankingNoteRepository;
        private readonly IBankingTransactionRepository _bankingTransactionRepository;

        public EfCoreExpenseRepository(IDbContextProvider<CashControlsDbContext> dbContextProvider, IWalletRepository walletRepository, IBankingNoteRepository bankingNoteRepository, IBankingTransactionRepository bankingTransactionRepository)
            : base(dbContextProvider)
        {
            _walletRepository = walletRepository;
            _bankingNoteRepository = bankingNoteRepository;
            _bankingTransactionRepository = bankingTransactionRepository;
        }

        public override async Task<Expense> InsertAsync(Expense entity, bool autoSave = false, CancellationToken cancellationToken = default)
        {
            CheckAndSetId(entity);

            var dbContext = await GetDbContextAsync();

            var savedEntity = (await dbContext.Expenses.AddAsync(entity, GetCancellationToken(cancellationToken))).Entity;

            if (savedEntity.WalletId != null)
            {
                var wallet = await _walletRepository.GetByIdAsync(savedEntity.WalletId.Value);

                if (wallet == null)
                {
                    var banking = await _bankingNoteRepository.GetAsync(x => x.Id == savedEntity.WalletId);

                    if (banking != null)
                    {
                        var transaction = await _bankingTransactionRepository.InsertAsync(new BankingNoteTransaction
                        {
                            Amount = savedEntity.Price,
                            BankingNoteId = savedEntity.WalletId.Value,
                            Date = savedEntity.Date,
                            TransactionType = TransactionTypeEnum.ChiTieu,
                        });

                        await _bankingNoteRepository.UpdateMoneyWhenCreateTransaction(transaction.BankingNoteId, transaction.Amount, "sub");
                    }
                }
                else
                {
                    await _walletRepository.ChangeMoney(wallet.Id, savedEntity.Price, "sub");
                }
            }


            if (autoSave)
            {
                await dbContext.SaveChangesAsync(GetCancellationToken(cancellationToken));
            }

            return savedEntity;
        }

        public async Task<long> GetCountAsync(
            string filterText = null,
            Range<DateTime>? date = null,
            Guid? categoryID = null,
            int? status = -1,
            CancellationToken cancellationToken = default)
        {
            var input = new GetExpensesInput
            {
                Filter = filterText,
                Date = date,
                CategoryID = categoryID,
                Status = status,
            };

            var query = await CreateFilteredQueryAsync(await GetQueryableAsync(), input);

            return await query.LongCountAsync(GetCancellationToken(cancellationToken));
        }

        public async Task<List<Expense>> GetListAsync(
            string filterText = null,
            Range<DateTime>? date = null,
            Guid? categoryID = null,
            int? status = -1,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string[] includes = null,
            CancellationToken cancellationToken = default)
        {
            var input = new GetExpensesInput
            {
                Filter = filterText,
                Date = date,
                CategoryID = categoryID,
                Status = status,
                Sorting = sorting,
                MaxResultCount = maxResultCount,
                SkipCount = skipCount,

            };

            var query = await CreateFilteredQueryAsync(await GetQueryableAsync(), input);

            if (includes != null)
            {
                query = query.Include(includes.First());

                foreach (var include in includes.Skip(1))
                {
                    query = query.Include(include);
                }
            }

            query = query.OrderByDescending(x => x.Date);

            return await query.PageBy(skipCount, maxResultCount).ToListAsync();
        }

        public async Task<decimal> GetTotalAmountAsync()
        {
            var totalAmount = (await GetDbSetAsync()).Sum(x => x.Price);

            return totalAmount;
        }

        protected async Task<IQueryable<Expense>> CreateFilteredQueryAsync(IQueryable<Expense> query, GetExpensesInput input)
        {
            return query.ApplyFilter(input);

            //return query
            //    .WhereIf(!string.IsNullOrWhiteSpace(filterText), e => e.Name.Contains(filterText))
            //    .WhereIf(!string.IsNullOrWhiteSpace(name), e => e.Name.Contains(name))
            //    .WhereIf(price.HasValue, e => e.Price == price)
            //    .WhereIf(date.HasValue, e => e.Date == date)
            //    .WhereIf(!string.IsNullOrWhiteSpace(categoryID), e => e.CategoryID.Equals(categoryID))
            //    .WhereIf(status == 0, e => e.Status == Status.SHOW)
            //    .WhereIf(status == 1, e => e.Status == Status.HIDE);
        }

        public async Task<decimal> GetTotalAmountMonthAsync(int month, int year)
        {
            var total = (await GetDbSetAsync())
                .Where(x => x.Date.Month == month && x.Date.Year == year)
                .Sum(x => x.Price);

            return total;
        }
    }
}
