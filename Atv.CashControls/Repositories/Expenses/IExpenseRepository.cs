﻿using Atv.CashControls.Entities;
using AutoFilterer.Types;
using Volo.Abp.Domain.Repositories;

namespace Atv.CashControls.Repositories.Expenses
{
    public interface IExpenseRepository : IRepository<Expense, Guid>
    {
        Task<decimal> GetTotalAmountAsync();
        Task<decimal> GetTotalAmountMonthAsync(int month, int year);

        Task<List<Expense>> GetListAsync(
            string filterText = null,
            Range<DateTime>? date = null,
            Guid? categoryID = null,
            int? status = -1,
            string sorting = null,
            int maxResultCount = int.MaxValue,
            int skipCount = 0,
            string[] includes = null,
            CancellationToken cancellationToken = default
        );

        Task<long> GetCountAsync(
            string filterText = null,
            Range<DateTime>? date = null,
            Guid? categoryID = null,
            int? status = -1,
            CancellationToken cancellationToken = default
        );
    }
}
