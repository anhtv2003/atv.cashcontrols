﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Ui.Branding;

namespace Atv.CashControls;

[Dependency(ReplaceServices = true)]
public class CashControlsBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "CashControls";
}
