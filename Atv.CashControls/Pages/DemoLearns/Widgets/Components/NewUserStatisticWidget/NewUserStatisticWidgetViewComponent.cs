﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Widgets;
using Volo.Abp.AspNetCore.Mvc;

namespace Atv.CashControls.Pages.Components.NewUserStatisticWidget
{
    [Widget(
        ScriptTypes = new[] { typeof(NewUserStatisticWidgetScriptContributor) }
        )]
    public class NewUserStatisticWidgetViewComponent : AbpViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
