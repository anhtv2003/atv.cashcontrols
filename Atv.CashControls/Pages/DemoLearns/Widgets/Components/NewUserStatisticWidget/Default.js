﻿(function () {
    var newUserStatisticWidget = abp.widgets.NewUserStatisticWidget = function ($wrapper) {

        var _latestFilters;
        var _chart;

        function formatDate(date) {
            return luxon.DateTime.fromISO(date, {
                locale: abp.localization.currentCulture.name
            }).toFormat('MM/dd/yyyy');
        }

        var getFilters = function () {
            return {
                startDate: $('#StartDate'),
                endDate: $('#EndDate'),
                frequency: $wrapper.find('.frequency-filter option:selected').val()
            };
        }

        var refresh = function (filters) {
            _latestFilters = {
                startDate: formatDate(filters.startDate),
                endDate: formatDate(filters.endDate),
                frequency: filters.frequency
            };

            atv.cashControls.controllers.dashboard.getNewUserStatisticWidget(_latestFilters)
                .then(function (result) {
                    _chart.data = {
                        labels: Object.keys(result.data),
                        datasets: [
                            {
                                label: 'User count',
                                data: Object.values(result.data),
                                backgroundColor: 'rgba(255, 132, 132, 1)'
                            }
                        ]
                    };
                    _chart.update();
                });
        };

        var init = function (filters) {
            _chart = new Chart($wrapper.find('.NewUserStatisticChart'),
                {
                    type: 'bar',
                    options: {
                        scales: {
                            yAxes: [
                                {
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }
                            ]
                        }
                    }
                });

            refresh(filters);

            $wrapper
                .find('.frequency-filter')
                .on('change',
                    function () {
                        refresh($.extend(_latestFilters, getFilters()));
                    });
        };

        return {
            getFilters: getFilters,
            init: init,
            refresh: refresh
        };
    };

    $(function () {
        var instance = newUserStatisticWidget($('#newUserStatisticWidgetWrapper'));
        instance.init(instance.getFilters());
    });
})();