﻿using Volo.Abp.AspNetCore.Mvc.UI.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Packages.ChartJs;
using Volo.Abp.Modularity;

namespace Atv.CashControls.Pages.Components.NewUserStatisticWidget
{
    [DependsOn(typeof(ChartjsScriptContributor))]
    public class NewUserStatisticWidgetScriptContributor : BundleContributor
    {
        public override void ConfigureBundle(BundleConfigurationContext context)
        {
            context.Files.Add("/Pages/DemoLearns/Widgets/Components/NewUserStatisticWidget/Default.js");
        }
    }
}
