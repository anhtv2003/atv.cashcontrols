﻿$(document).ready(function () {
    var licenseStatisticWidget = abp.widgets.LicenseStatisticWidget = function ($wrapper) {

        var _chart;

        var getFilters = function () {
            return {
                startDate: $('#StartDate').val(),
                endDate: $('#EndDate').val()
            };
        }

        var refreshChart = function (statistic) {
            _chart.data = {
                labels: Object.keys(statistic.data),
                datasets: [
                    {
                        label: 'License ratios',
                        data: Object.values(statistic.data),
                        backgroundColor: [
                            'rgba(50, 150, 255, 1)',
                            'rgba(150, 255, 100, 1)',
                            'rgba(255, 100, 150, 1)'
                        ]
                    }
                ]
            };
            _chart.update();
        };

        var render = function (filters, callback) {
            abp.ajax({
                url: `/api/cash-controls/dashboard/get-license-statistic-widget?startDate=${filters.startDate}&endDate=${filters.endDate}`,
                type: 'GET'
            }).done(function (result) {
                callback(result);
            });
        };

        var refresh = function (filters) {
            render(filters, refreshChart);
        };

        var init = function (filters) {
            _chart = new Chart($wrapper.find('.LicenseStatisticChart'),
                {
                    type: 'pie'
                });

            refresh(filters);
        };

        return {
            getFilters: getFilters,
            init: init,
            refresh: refresh
        };
    };

    var instance = licenseStatisticWidget($('#licenseStatisticWidgetWrapper'));
    instance.init(instance.getFilters());
});