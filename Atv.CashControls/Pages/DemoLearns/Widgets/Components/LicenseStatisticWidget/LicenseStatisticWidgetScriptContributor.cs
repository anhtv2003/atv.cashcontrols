﻿using Volo.Abp.AspNetCore.Mvc.UI.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Packages.ChartJs;
using Volo.Abp.Modularity;

namespace Atv.CashControls.Pages.Components.LicenseStatisticWidget
{
    [DependsOn(typeof(ChartjsScriptContributor))]
    public class LicenseStatisticWidgetScriptContributor : BundleContributor
    {
        public override void ConfigureBundle(BundleConfigurationContext context)
        {
            context.Files.Add("/libs/chart.js/chart.umd.js");
            context.Files.Add("/Pages/DemoLearns/Widgets/Components/LicenseStatisticWidget/Default.js");
        }
    }
}
