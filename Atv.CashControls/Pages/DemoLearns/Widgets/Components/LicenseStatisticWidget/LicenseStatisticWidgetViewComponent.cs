﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Widgets;
using Volo.Abp.AspNetCore.Mvc;
using Atv.CashControls.Services.Dashboard;
using Atv.CashControls.Dtos.Dashboard;

namespace Atv.CashControls.Pages.Components.LicenseStatisticWidget
{
    [Widget( 
        ScriptTypes = new[] { typeof(LicenseStatisticWidgetScriptContributor) }
        )]
    public class LicenseStatisticWidgetViewComponent : AbpViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
