﻿using Atv.CashControls.Dtos.Dashboard;
using Atv.CashControls.Services.Dashboard;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Widgets;

namespace Atv.CashControls.Pages.Components.CountersWidget
{
    [Widget(
        StyleFiles = new[] { "/Pages/DemoLearns/Widgets/Components/CountersWidget/Default.css" },
        RefreshUrl = "Widgets/Counters"
        )]
    public class CountersWidgetViewComponent : AbpViewComponent
    {
        private readonly IDashboardService _dashboardService;

        public CountersWidgetViewComponent(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        public async Task<IViewComponentResult> InvokeAsync(DateTime startDate, DateTime endDate)
        {
            var result = await _dashboardService.GetCountersWidgetAsync(
                new CountersWidgetInputDto
                {
                    StartDate = startDate,
                    EndDate = endDate
                }
            );

            return View(result);
        }
    }
}
