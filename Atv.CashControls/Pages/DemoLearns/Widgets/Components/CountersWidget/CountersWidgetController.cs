﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Atv.CashControls.Pages.Components.CountersWidget
{
    [Route("Widgets")]
    public class CountersWidgetController : AbpController
    {
        [HttpGet]
        [Route("Counters")]
        public IActionResult Counters(DateTime startDate, DateTime endDate)
        {
            return ViewComponent("CountersWidget", new { startDate, endDate });
        }
    }
}
