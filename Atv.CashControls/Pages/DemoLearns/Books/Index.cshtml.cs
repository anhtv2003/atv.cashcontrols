using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Books
{
    public class Index : AbpPageModel
    {
        private readonly ILogger<Index> _logger;

        public Index(ILogger<Index> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
    }
}