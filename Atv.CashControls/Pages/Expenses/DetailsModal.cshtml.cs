using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Entities;
using Atv.CashControls.Services.Expenses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Expenses
{
    public class DetailsModalModel : AbpPageModel
    {
        private readonly IExpenseService _expenseService;

        public DetailsModalModel(IExpenseService expenseService)
        {
            _expenseService = expenseService;
        }

        [BindProperty(SupportsGet = true)]
        public Guid Id { get; set; }

        public ExpenseDto ExpenseDto { get; set; }

        public async Task OnGetAsync()
        {
            ExpenseDto = await _expenseService.GetAsync(Id);
        }
    }
}
