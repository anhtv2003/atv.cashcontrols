using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Services.CashCategories;
using Atv.CashControls.Services.Expenses;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Expenses
{
    public class IndexModel : AbpPageModel
    {
        private readonly IExpenseService _service;
        private readonly ICashCategoryService _cashCategoryService;

        public IndexModel(IExpenseService service, ICashCategoryService cashCategoryService)
        {
            _service = service;
            _cashCategoryService = cashCategoryService;
        }

        [BindProperty]
        public IEnumerable<CashCategoryDto> CategoryDto { get; set; }

        public long Count { get; set; }

        public async Task OnGetAsync()
        {
            Count = await _service.GetCount();

            CategoryDto = (await _cashCategoryService.GetListAync(new GetCashCategoriesInput { FilterText = string.Empty, Name = string.Empty })).Items;

            await Task.CompletedTask;
        }
    }
}
