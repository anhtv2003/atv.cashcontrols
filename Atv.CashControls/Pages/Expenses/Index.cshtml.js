﻿$(function () {
    var l = abp.localization.getResource("CashControls");

    var createModal = new abp.ModalManager(abp.appPath + "Expenses/CreateModal");
    var editModal = new abp.ModalManager(abp.appPath + "Expenses/EditModal");
    var detailsModal = new abp.ModalManager(abp.appPath + "Expenses/DetailsModal");
    var categoryCreateModal = new abp.ModalManager(abp.appPath + "CashCategories/CreateModal");

    $(document).on("click", '#btnCreateCategory', function () {
        categoryCreateModal.open();
    });

    $(document).on('input', '#ExpenseVM_Price', function () {
        var value = $(this).val().replace(/\D/g, '');

        if (value) {
            value = new Intl.NumberFormat('de-DE').format(value);
            $(this).val(value);
        }
    })

    // Loại bỏ dấu chấm trước khi submit form
    $(document).on('submit', '#createExpenseModal', function () {
        var priceInput = $('#ExpenseVM_Price');
        var rawValue = priceInput.val().replace(/\./g, ''); // Loại bỏ dấu phân cách
        priceInput.val(rawValue); // Gán giá trị không có dấu phân cách lại cho input
    });

    $("#createExpenseBtn").on("click", function (e) {
        e.preventDefault();
        createModal.open();
    });

    createModal.onResult(function () {
        loadCount();
        abp.notify.success(l("SuccessfullyCreated"));
        dataTable.ajax.reload();
    });

    editModal.onResult(function () {
        abp.notify.success(l("SuccessfullyUpdated"));
        dataTable.ajax.reload();
    });

    function loadCount() {
        abp.ajax({
            url: "/api/cash-controls/expenses/expense-count",
            type: "GET",
        })
            .done(function (res) {
                if (res.success) {
                    $("#expense-count").text(res.data);
                }
            })
            .fail(function (err) {
                console.error("Error fetching expense count:", err);
            });
    }
    loadCount();

    function addCell(tr, content, colSpan = 1) {
        let td = $('<th/>', {
            'colspan': colSpan,
            'text': content
        });

        $(tr).append(td);
    }

    function formatDate(date) {
        return luxon.DateTime.fromISO(date, {
            locale: abp.localization.currentCulture.name
        }).toLocaleString(luxon.DateTime.DATE_SHORT);
    }

    function formatPrice(price) {
        var currentName = abp.localization.currentCulture.name;

        return new Intl.NumberFormat(currentName, {
            style: "currency",
            currency: currentName === "vi" ? "VND" : "USD",
        }).format(price);
    }

    var getFilter = () => {
        const formatAdvDate = (dateStr) => {
            return luxon.DateTime.fromFormat(dateStr, 'dd/MM/yyyy').toISODate();
        }

        return {
            categoryID: $('#advanceCategory').val(),
            date: {
                min: $('#advanceDateMin').val() ? formatAdvDate($('#advanceDateMin').val()) : null,
                max: $('#advanceDateMax').val() ? formatAdvDate($('#advanceDateMax').val()) : null
            },
        }
    }

    $('#btn_AdvanceSearch').on('click', () => {
        dataTable.ajax.reload();
    })

    $('#btn_AdvanceCancel').on('click', () => {
        $('#advanceCategory').val("");
        $('#advanceDateMin').val("");
        $('#advanceDateMax').val("");

        dataTable.ajax.reload();
    })

    var dataTable = $("#expenseTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            processing: true,
            serverSide: true,
            select: true,
            paging: true,
            pageLength: 25,
            order: [[5, "desc"]],
            searching: false,
            responsive: true,
            scrollX: true,
            scrollCollapse: true,
            rowGroup: {
                startRender: function (rows, group, level) {
                    let tr = $('<tr class="group-background"/>');

                    if (level == 0) {
                        tr = $('<tr class="group-0-background"/>');
                        var str = group.split('/');
                        var month = str[0];
                        var year = str[1];

                        abp.ajax({
                            url: `/api/cash-controls/expenses/get-total-amount-month/${month}/${year}`,
                            type: "GET",
                        })
                            .done(function (res) {
                                addCell(tr, group, 2);
                                addCell(tr, formatPrice(res), 6);
                            })
                            .fail(function (err) {
                                console.error("Error fetching expense total:", err);
                            });                        

                    } else if (level === 1) { // Nhóm theo ngày
                        let total_price_group = rows.data().pluck('price').reduce((a, b) => { return a + parseFloat(b) }, 0);

                        let day = formatDate(group);

                        addCell(tr, day, 2); // Hiển thị ngày
                        addCell(tr, formatPrice(total_price_group), 6);
                    }

                    return tr;
                },
                dataSrc: ['month', 'date']
            },
            ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.expense.getListAync, getFilter),
            columnDefs: [
                //{
                //    title: '',
                //    data: 'id',
                //    render: function (data) {
                //        return `<div class="form-check">
                //                  <input class="form-check-input" type="checkbox" value="${data}">
                //                </div>`;
                //    }
                //},
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Details"),
                                iconClass: "fa fa-info iconCustomClass",
                                action: function (data) {
                                    detailsModal.open({ id: data.record.id });
                                },
                            },
                            {
                                text: l("Edit"),
                                iconClass: "fas fa-pen-alt text-primary iconCustomClass",
                                visible: abp.auth.isGranted("CashControls.Expenses.Edit"),
                                action: function (data) {
                                    editModal.open({ id: data.record.id });
                                },
                            },
                            {
                                text: l("Delete"),
                                iconClass: "fas fa-trash-alt text-danger iconCustomClass",
                                visible: abp.auth.isGranted("CashControls.Expenses.Delete"),
                                confirmMessage: function () {
                                    return l("DeleteConfirmMessage");
                                },
                                action: function (data) {
                                    atv.cashControls.controllers.expense.delete(data.record.id).then(function () {
                                        abp.notify.success(l("SuccessfullyDeleted"));
                                        loadCount();
                                        dataTable.ajax.reload();
                                    });
                                },
                            },
                        ],
                    },
                },
                {
                    title: l("Name"),
                    orderable: true,
                    data: "name",
                },
                {
                    title: l("Price"),
                    orderable: true,
                    data: "price",
                    render: function (data) {
                        var currentName = abp.localization.currentCulture.name;

                        return new Intl.NumberFormat(currentName, {
                            style: "currency",
                            currency: currentName === "vi" ? "VND" : "USD",
                        }).format(data);
                    },
                },
                {
                    title: l("Quantity"),
                    orderable: true,
                    data: "quantity",
                },
                {
                    title: l("Category_Name"),
                    orderable: true,
                    data: "categoryName",
                },
                {
                    title: l("Date"),
                    orderable: true,
                    data: "date",
                    render: function (data) {
                        return luxon.DateTime.fromISO(data, {
                            locale: abp.localization.currentCulture.name,
                        }).toLocaleString(luxon.DateTime.DATE_SHORT);
                    },
                },
                {
                    title: l('Month'),
                    visible: false,
                    data: 'month',
                },
                {
                    title: l("User_Name"),
                    orderable: true,
                    data: "creatorId",
                    render: function (data, type, row, meta) {
                        var username = `<span id="user-name-${meta.row}">Loading ...</span>`;

                        abp.ajax({
                            url: `/api/identity/users/${data}`,
                            type: 'GET'
                        })
                            .then(function (res) {
                                $('#user-name-' + meta.row).text(res.userName);
                            })
                            .catch(function (err) {
                                console.error(err);
                                $('#user-name-' + meta.row).text('Error loading');
                            });

                        return username;

                    }
                },
                {
                    title: l("Status"),
                    orderable: true,
                    data: "status",
                    render: function (data) {
                        return data === 0 ? `<span class="badge text-bg-primary">${l("Enum:Status." + data)}</span>` : `<span class="badge text-bg-danger">${l("Enum:Status." + data)}</span>`;
                    },
                },
            ],
            footerCallback: function (row, data, start, end, display) {
                var api = this.api();
                var month = new Date().getMonth() + 1;

                abp.ajax({
                    url: `/api/cash-controls/expenses/get-total-amount-month/${month}/2024`,
                    type: "GET",
                })
                    .done(function (res) {
                        // Cập nhật tổng cho tháng vào cột thứ 2 (index 1)
                        $(api.column(2).footer()).html(
                            new Intl.NumberFormat(abp.localization.currentCulture.name, {
                                style: "currency",
                                currency: "VND",
                            }).format(res)
                        );
                    })
                    .fail(function (err) {
                        console.error("Error fetching expense total:", err);
                    });

                //
                abp.ajax({
                    url: "/api/cash-controls/expenses/get-total-amount",
                    type: "GET",
                })
                    .done(function (res) {
                        // Cập nhật tổng cộng vào dòng thứ 2, cột thứ 2 (index 1)
                        $('tfoot #ftable-amount th:eq(1)').html(
                            new Intl.NumberFormat(abp.localization.currentCulture.name, {
                                style: "currency",
                                currency: "VND",
                            }).format(res)
                        );
                    })
                    .fail(function (err) {
                        console.error("Error fetching expense total:", err);
                    });
            },
        })
    );


});
