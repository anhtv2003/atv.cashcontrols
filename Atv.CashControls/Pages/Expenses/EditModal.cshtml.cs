using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Dtos.Wallets;
using Atv.CashControls.Entities;
using Atv.CashControls.Services.CashCategories;
using Atv.CashControls.Services.Expenses;
using Atv.CashControls.Services.Wallets;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Expenses
{
    public class EditModalModel : AbpPageModel
    {
        private readonly IExpenseService _expenseService;
        private readonly ICashCategoryService _cashCategoryService;
        private readonly IWalletService _walletService;

        public EditModalModel(IExpenseService expenseService, ICashCategoryService cashCategoryService, IWalletService walletService)
        {
            _expenseService = expenseService;
            _cashCategoryService = cashCategoryService;
            _walletService = walletService;
        }

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid Id { get; set; }

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid UserId { get; set; }

        [BindProperty]
        public EditExpenseViewModel ExpenseVM { get; set; }

        [BindProperty]
        public IEnumerable<CashCategoryDto> Categories { get; set; }

        [BindProperty]
        public IEnumerable<WalletDto> WalletsList { get; set; }

        public async Task OnGetAsync()
        {
            var expense = await _expenseService.GetAsync(Id);

            ExpenseVM = ObjectMapper.Map<ExpenseDto, EditExpenseViewModel>(expense);
            WalletsList = (await _walletService.GetListAync(new GetWalletInput { FilterText = string.Empty })).Items;

            var categPaged = await _cashCategoryService.GetListAync(new GetCashCategoriesInput(string.Empty, string.Empty));
            Categories = categPaged.Items;
        }

        public async Task<NoContentResult> OnPostAsync()
        {
            var expense = ObjectMapper.Map<EditExpenseViewModel, UpdateExpenseDto>(ExpenseVM);

            await _expenseService.UpdateAsync(Id, expense);

            return NoContent();
        }
    }

    public class EditExpenseViewModel : UpdateExpenseDto
    {

    }
}
