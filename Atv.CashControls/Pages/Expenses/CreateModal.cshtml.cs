using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Dtos.Wallets;
using Atv.CashControls.Entities;
using Atv.CashControls.Services.CashCategories;
using Atv.CashControls.Services.Expenses;
using Atv.CashControls.Services.Wallets;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Users;

namespace Atv.CashControls.Pages.Expenses
{
    public class CreateModalModel : AbpPageModel
    {
        private readonly IExpenseService _service;
        private readonly ICurrentUser _currentUser;
        private readonly ICashCategoryService _categoryService;
        private readonly IWalletService _WalletService;

        [BindProperty]
        public ExpensesViewModal ExpenseVM { get; set; }

        [BindProperty]
        [HiddenInput]
        public Guid UserId { get; set; }

        public CashCategoryDto SelectedCashCategory { get; set; }

        [BindProperty]
        public IEnumerable<CashCategoryDto> CategoriesList { get; set; }

        [BindProperty]
        public IEnumerable<WalletDto> WalletsList { get; set; }

        public CreateModalModel(IExpenseService service, ICashCategoryService categoryService, IWalletService walletService, ICurrentUser currentUser)
        {
            _categoryService = categoryService;
            _service = service;
            _currentUser = currentUser;
            _WalletService = walletService;
        }

        public async Task OnGet()
        {
            UserId = _currentUser.GetId();
            ExpenseVM = new ExpensesViewModal();

            CategoriesList = (await _categoryService.GetListAync());
            WalletsList = (await _WalletService.GetListAync(new GetWalletInput { FilterText = string.Empty })).Items;

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var entity = ObjectMapper.Map<ExpensesViewModal, CreateExpenseDto>(ExpenseVM);

            await _service.CreateAsync(entity);

            return NoContent();
        }
    }

    public class ExpensesViewModal : CreateExpenseDto
    {

    }
}
