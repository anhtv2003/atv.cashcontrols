using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Services.BankingNotes;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Users;

namespace Atv.CashControls.Pages.BankingNotes
{
    public class TransactionCreateModal : AbpPageModel
    {
        private readonly ILogger<TransactionCreateModal> _logger;
        private readonly IBankingTransactionService _transactionService;
        private readonly ICurrentUser _currentUser;

        public TransactionCreateModal(IBankingTransactionService transactionService, ICurrentUser currentUser, ILogger<TransactionCreateModal> logger)
        {
            _currentUser = currentUser;
            _transactionService = transactionService;
            _logger = logger;
        }

        [BindProperty]
        public CreateBankingTransactionViewModel TransactionVM { get; set; }

        public async Task OnGetAsync()
        {
            TransactionVM = new CreateBankingTransactionViewModel();

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var dto = ObjectMapper.Map<CreateBankingTransactionViewModel, CreateBankingNoteTransactionDto>(TransactionVM);

            await _transactionService.CreateAsync(dto);

            return NoContent();
        }
    }

    public class CreateBankingTransactionViewModel : CreateBankingNoteTransactionDto
    {

    }
}