﻿using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Entities;
using Atv.CashControls.Services.BankingNotes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.BankingNotes
{
    public class TransactionUpdateModalModel : AbpPageModel
    {
        private readonly IBankingTransactionService _bankingTransactionService;
        private readonly ILogger<TransactionUpdateModalModel> _logger;

        public TransactionUpdateModalModel(IBankingTransactionService bankingTransactionService, ILogger<TransactionUpdateModalModel> logger)
        {
            _bankingTransactionService = bankingTransactionService;
            _logger = logger;
        }

        [BindProperty(SupportsGet = true)]
        public Guid Id { get; set; }

        [BindProperty(SupportsGet = true)]
        public Guid BankingNoteId { get; set; }

        [BindProperty]
        public UpdateBankingTransactionViewModel TransactionVM { get; set; }

        public async Task OnGetAsync()
        {
            var dto = await _bankingTransactionService.GetAsync(Id, BankingNoteId);
            if (dto == null)
            {
                return;
            }

            TransactionVM = ObjectMapper.Map<BankingNoteTransactionDto, UpdateBankingTransactionViewModel>(dto);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var dto = ObjectMapper.Map<UpdateBankingTransactionViewModel, UpdateBankingNoteTransactionDto>(TransactionVM);

            await _bankingTransactionService.UpdateAsync(Id, TransactionVM);

            _logger.LogInformation("Cập nhật thành công!");

            return NoContent();
        }


    }

    public class UpdateBankingTransactionViewModel : UpdateBankingNoteTransactionDto { }
}
