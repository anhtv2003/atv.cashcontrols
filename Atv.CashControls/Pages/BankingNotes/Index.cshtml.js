﻿$(function () {
    const l = abp.localization.getResource("CashControls");

    const createModal = new abp.ModalManager(abp.appPath + "BankingNotes/CreateModal");
    const updateModal = new abp.ModalManager(abp.appPath + 'BankingNotes/UpdateModal');
    const transactionCreateModal = new abp.ModalManager(abp.appPath + "BankingNotes/TransactionCreateModal");
    const transactionUpdateModal = new abp.ModalManager(abp.appPath + "BankingNotes/TransactionUpdateModal");

    let bankingNoteId = "";
    let transactionMoney = 0;

    $("#createModalBtn").on("click", function (e) {
        e.preventDefault();
        createModal.open();
    });

    createModal.onResult(function () {
        abp.notify.success(l("SuccessfullyCreated"));
        dataTable.ajax.reload();
    });

    updateModal.onResult(() => {
        abp.notify.success(l("SuccessfullyUpdated"));
        dataTable.ajax.reload();
    })

    transactionCreateModal.onOpen(function () {
        console.log(bankingNoteId);

        $("#TransactionVM_BankingNoteId").val(bankingNoteId);
    });

    transactionCreateModal.onResult(function () {
        abp.notify.success(l("SuccessfullyCreated"));

        const price = $('#TransactionVM_Amount').val();
        const type = $('#TransactionType').val();
        let method = 'add';

        if (type === '1') method = 'sub';
        else if (type === '2') method = 'sub';
        else if (type === '3') method = 'add';

        abp.ajax({
            url: `/api/cash-controls/banking-notes/update-money-transaction/${bankingNoteId}?price=${price}&method=${method}`,
            type: 'POST'
        }).done((res) => {
            dataTable.ajax.reload();
        }).fail(function (err) {
            console.error("Error fetching expense count:", err);
        });

        transactionsDataTable.ajax.reload();
    });

    transactionUpdateModal.onResult(function () {
        abp.notify.success(l("SuccessfullyUpdated"));

        transactionsDataTable.ajax.reload();
    })

    //============= From event =============
    // Create
    $(document).on('input', '#BankingNoteVM_Money', function () {
        var value = $(this).val().replace(/\D/g, '');

        if (value) {
            value = new Intl.NumberFormat('de-DE').format(value);
            $(this).val(value);
        }
    })

    // Loại bỏ dấu chấm trước khi submit form
    $(document).on('submit', '#frmCreateBankingNote', function () {
        var priceInput = $('#BankingNoteVM_Money');
        var rawValue = priceInput.val().replace(/\./g, ''); // Loại bỏ dấu phân cách
        priceInput.val(rawValue); // Gán giá trị không có dấu phân cách lại cho input
    });

    // Update
    $(document).on('input', '#TransactionVM_Amount', function () {
        var value = $(this).val().replace(/\D/g, '');

        if (value) {
            value = new Intl.NumberFormat('de-DE').format(value);
            $(this).val(value);
        }
    })

    // Loại bỏ dấu chấm trước khi submit form
    $(document).on('submit', '#frmCreateTransaction', function () {
        var priceInput = $('#TransactionVM_Amount');
        var rawValue = priceInput.val().replace(/\./g, ''); // Loại bỏ dấu phân cách
        priceInput.val(rawValue); // Gán giá trị không có dấu phân cách lại cho input
    });

    //============= End form event =============

    const getFilter = function () {
        return {
            filterText: "",
        };
    };

    const formatDateTime = (data) => {
        return luxon.DateTime.fromISO(data, {
            locale: abp.localization.currentCulture.name,
        }).toLocaleString(luxon.DateTime.DATE_SHORT);
    }

    var dataTable = $("#bankingNotesDatatable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            processing: true,
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            scrollCollapse: true,
            ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.bankingNote.getList, getFilter),
            columnDefs: [
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                iconClass: "fas fa-pen-alt text-primary",
                                visible: abp.auth.isGranted("CashControls.BankingNotes.Edit"),
                                action: function (data) {
                                    updateModal.open({ id: data.record.id });
                                },
                            },
                            {
                                text: l("Delete"),
                                iconClass: "fas fa-trash-alt text-danger",
                                visible: abp.auth.isGranted("CashControls.BankingNotes.Delete"),
                                confirmMessage: function () {
                                    return l("DeleteConfirmMessage");
                                },
                                action: function (data) {
                                    atv.cashControls
                                        .controllers
                                        .bankingNote
                                        .delete(data.record.id)
                                        .then(function () {
                                            abp.notify.success(l("SuccessfullyDeleted"));
                                            dataTable.ajax.reload();
                                            if (transactionsDataTable) {
                                                transactionsDataTable.ajax.reload();
                                            }
                                        })
                                        .catch(function (error) {
                                            console.error("Error details:", error);
                                            abp.notify.error(`Error: ${error.message} (Code: ${error.code})`);
                                        });
                                },
                            },
                        ],
                    },
                },
                {
                    title: l("Name"),
                    data: "bankingName",
                },
                {
                    title: l("Money"),
                    data: "money",
                    render: function (data) {
                        var currentName = abp.localization.currentCulture.name;

                        return new Intl.NumberFormat(currentName, {
                            style: "currency",
                            currency: currentName === "vi" ? "VND" : "USD",
                        }).format(data);
                    },
                },
                {
                    title: "",
                    data: "id",
                    render: function (data, type, row, meta) {
                        return `
                        <a href="#banking-row-${meta.row}" class="btn text-primary btn-check-transaction-details" data-id="${data}">
                            <i class="fa-solid fa-arrow-up-right-from-square"></i>
                        </a>
                        `;
                    },
                },
            ],
        })
    );

    var transactionsDataTable;

    $("#createTransactionDetails").on("click", function () {
        if (transactionsDataTable) {
            transactionCreateModal.open();
            // $("#TransactionVM_BankingNoteId").val(bankingNoteId);
        } else {
            abp.notify.success("Cần chọn một ngân hàng!");
        }
    });

    function addCell(tr, content, colSpan = 1) {
        let td = $('<th/>', {
            'colspan': colSpan,
            'text': content
        });

        $(tr).append(td);
    }

    $(document).on("click", ".btn-check-transaction-details", function () {
        var noteid = $(this).data("id");
        bankingNoteId = noteid;

        $("#bankingNotesDatatable tbody tr").removeClass("active");
        $(this).parent().parent().addClass("active");

        var getInput = function () {
            return {
                noteId: noteid,
                filter: $("#FilterText").val(),
            };
        };

        if (transactionsDataTable) {
            transactionsDataTable.ajax.url(abp.libs.datatables.createAjax(atv.cashControls.controllers.bankingTransactions.getList, getInput)).load();
        } else {
            transactionsDataTable = $("#transactionDetails").DataTable(
                abp.libs.datatables.normalizeConfiguration({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    order: [[1, "asc"]],
                    searching: false,
                    scrollX: true,
                    scrollCollapse: true,
                    rowGroup: {
                        startRender: function (rows, group) {
                            let tr = $('<tr/>');

                            addCell(tr, formatDateTime(group), 9);

                            return tr;
                        },
                        dataSrc: 'date'
                    },
                    ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.bankingTransactions.getList, getInput),
                    columnDefs: [
                        {
                            title: l("Actions"),
                            rowAction: {
                                items: [
                                    {
                                        text: l("Edit"),
                                        iconClass: "fas fa-pen-alt text-primary",
                                        visible: abp.auth.isGranted("CashControls.BankingNotes.Edit"),
                                        action: function (data) {
                                            console.log(data);
                                            transactionUpdateModal.open({ id: data.record.id, bankingNoteId: data.record.bankingNoteId });
                                        },
                                    },
                                    {
                                        text: l("Delete"),
                                        iconClass: "fas fa-trash-alt text-danger",
                                        visible: abp.auth.isGranted("CashControls.BankingNotes.Delete"),
                                        confirmMessage: function () {
                                            return l("DeleteConfirmMessage");
                                        },
                                        action: function (data) {
                                            atv.cashControls.controllers.bankingTransactions.delete(data.record.id)
                                                .then(function () {
                                                    abp.notify.success(l("SuccessfullyDeleted"));
                                                    dataTable.ajax.reload();
                                                    if (transactionsDataTable) {
                                                        transactionsDataTable.ajax.reload();
                                                    }
                                                })
                                                .catch(function (error) {
                                                    console.error("Error details:", error);
                                                    abp.notify.error(`Error: ${error.message} (Code: ${error.code})`);
                                                });
                                        },
                                    },
                                ],
                            },
                        },
                        {
                            title: l("User_Name"),
                            orderable: true,
                            data: "creatorId",
                            render: function (data, type, row, meta) {
                                var username = `<span id="user-name-${meta.row}">Loading ...</span>`;

                                abp.ajax({
                                    url: `/api/identity/users/${data}`,
                                    type: 'GET'
                                })
                                    .then(function (res) {
                                        $('#user-name-' + meta.row).text(res.userName);
                                    })
                                    .catch(function (err) {
                                        console.error(err);
                                        $('#user-name-' + meta.row).text('Error loading');
                                    });

                                return username;

                            }
                        },
                        {
                            title: l("Amount"),
                            data: "amount",
                            render: function (data, type, row) {
                                var currentName = abp.localization.currentCulture.name;

                                var AmountFormat = new Intl.NumberFormat(currentName, {
                                    style: "currency",
                                    currency: currentName === "vi" ? "VND" : "USD",
                                }).format(data);

                                var renderAmount = "";

                                if (row.transactionType === 1) {
                                    renderAmount = `<span class="text-danger">- ${AmountFormat}</span>`;
                                } else if (row.transactionType === 3) {
                                    renderAmount = `<span class="text-success">+ ${AmountFormat}</span>`;
                                } else if (row.transactionType === 2) {
                                    renderAmount = `<span class="text-warning">-> ${AmountFormat}</span>`;
                                } else if (row.transactionType === 4) {
                                    renderAmount = `<span class="text-danger">- ${AmountFormat}</span>`;
                                }

                                return renderAmount;
                            },
                        },
                        {
                            title: l("Date"),
                            data: "date",
                            render: function (data) {
                                return formatDateTime(data)
                            },
                        },
                        {
                            title: l("Transaction_Type"),
                            data: "transactionType",
                            render: function (data) {
                                return `<span>${l('Enum:TransactionTypeEnum.' + data)}</span>`;
                            },
                        },
                        {
                            title: l("Place"),
                            data: 'place'
                        },
                        {
                            title: l("Banking_Receive"),
                            data: "bankingReceive",
                        },
                        {
                            title: l("Receiver"),
                            data: "receiver",
                        },
                        {
                            title: l("Remitter"),
                            data: "remitter",
                        },
                    ],
                })
            );
        }
    });

    $("#transactions-filter-wrapper :input").on("input", function () {
        if (transactionsDataTable) {
            transactionsDataTable.ajax.reload();
        }
    });

    $(document).on('input', '#TransactionVM_Amount', function () {
        let type = $('#TransactionType').val();
        let amount = $(this).val();


    });

    $(document).on("change", "#TransactionType", function () {
        let type = $(this).val();

        if (type === "1") {
            $(".choiceType").removeClass("show").addClass("hide");
            $("#choiceRutTien").removeClass("hide").addClass("show");
        } else if (type === "2") {
            $(".choiceType").removeClass("show").addClass("hide");
            $("#choiceChuyenTien").removeClass("hide").addClass("show");
        } else if (type === "3") {
            $(".choiceType").removeClass("show").addClass("hide");
            $("#choiceNhanTien").removeClass("hide").addClass("show");
        }
    });
});
