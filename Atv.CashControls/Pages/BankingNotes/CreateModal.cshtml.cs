using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Services.BankingNotes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Users;

namespace Atv.CashControls.Pages.BankingNotes
{
    public class CreateModalModel : AbpPageModel
    {
        private readonly IBankingNoteService _bankingNoteService;
        private readonly ICurrentUser _currentUser;

        public CreateModalModel(IBankingNoteService bankingNoteService, ICurrentUser currentUser)
        {
            _bankingNoteService = bankingNoteService;
            _currentUser = currentUser;
        }

        [BindProperty]
        public CreateBankingNoteViewModel BankingNoteVM { get; set; }

        public async Task OnGetAsync()
        {
            BankingNoteVM = new CreateBankingNoteViewModel();
            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var entityDto = ObjectMapper.Map<CreateBankingNoteViewModel, CreateBankingNoteDto>(BankingNoteVM);

            await _bankingNoteService.CreateAsync(entityDto);

            return NoContent();
        }
    }

    public class CreateBankingNoteViewModel : CreateBankingNoteDto
    {

    }
}
