using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Services.BankingNotes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.BankingNotes
{
    public class UpdateModalModel : AbpPageModel
    {
        private readonly IBankingNoteService _bankingNoteService;

        public UpdateModalModel(IBankingNoteService bankingNoteService)
        {
            _bankingNoteService = bankingNoteService;
        }

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid Id { get; set; }

        [BindProperty]
        public UpdateBankingNoteViewModel BankingNoteVM { get; set; }

        public async Task OnGetAsync()
        {
            var dto = await _bankingNoteService.GetAsync(Id);

            BankingNoteVM = ObjectMapper.Map<BankingNoteDto, UpdateBankingNoteViewModel>(dto);

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var dto = ObjectMapper.Map<UpdateBankingNoteViewModel, UpdateBankingNoteDto>(BankingNoteVM);

            await _bankingNoteService.UpdateAsync(Id, dto);

            return NoContent();
        }
    }

    public class UpdateBankingNoteViewModel : UpdateBankingNoteDto { }
}
