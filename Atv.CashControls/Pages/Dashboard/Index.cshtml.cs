using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Dashboard
{
    public class IndexModel : AbpPageModel
    {
        [BindProperty(SupportsGet = true)]
        public DateTime StartDate { get; set; }

        [BindProperty(SupportsGet = true)]
        public DateTime EndDate { get; set; }

        public void OnGet()
        {
            StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ClearTime();

            EndDate = DateTime.Now.AddDays(1).ClearTime();
        }
    }
}
