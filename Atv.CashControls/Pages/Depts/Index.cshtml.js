﻿$(document).ready(function () {
    var l = abp.localization.getResource('CashControls');

    const createModal = new abp.ModalManager(abp.appPath + 'Depts/CreateModal');

    $('#btn_CreateDept').on('click', function (e) {
        e.preventDefault();
        createModal.open();
    });

    createModal.onResult(function () {
        abp.notify.success(l('SuccessfullyCreated'));
        datatable.ajax.reload();
    })

    var datatable = $('#deptDatatable').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            processing: true,
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            scrollCollapse: true,
            ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.dept.getList),
            columnDefs: [
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                iconClass: "fas fa-pen-alt text-primary",
                                visible: abp.auth.isGranted('CashControls.Depts.Edit'),
                                action: function (data) {
                                    //editModal.open({ id: data.record.id });
                                }
                            },
                            {
                                text: l("Delete"),
                                iconClass: "fas fa-trash-alt text-danger",
                                visible: abp.auth.isGranted('CashControls.Depts.Delete'),
                                confirmMessage: function () {
                                    return l("DeleteConfirmMessage");
                                },
                                action: function (data) {
                                    atv.cashControls.controllers.dept
                                        .delete(data.record.id)
                                        .then(function () {
                                            abp.notify.success(l('SuccessfullyDeleted'));
                                            datatable.ajax.reload();
                                        })
                                }
                            }

                        ]
                    }
                },
                {
                    title: l('Price'),
                    data: "price",
                    render: function (data) {
                        var currentName = abp.localization.currentCulture.name;

                        return new Intl.NumberFormat(currentName, {
                            style: 'currency',
                            currency: currentName === 'vi' ? 'VND' : 'USD'
                        }).format(data);
                    }
                },
                {
                    title: l('Lender'),
                    data: 'lender'
                },
                {
                    title: l('Date'),
                    data: "date",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATE_SHORT);
                    }
                },
                {
                    title: l('DueDate'),
                    data: "dueDate",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATE_SHORT);
                    }
                },
                {
                    title: l('Status'),
                    data: 'status',
                    render: function (data) {
                        return data === 0 ?
                            `<span class="badge text-bg-danger">${l('Enum:DeptStatusEnum.' + data)}</span>` :
                            `<span class="badge text-bg-success text-white">${l('Enum:DeptStatusEnum.' + data)}</span>`;
                    }
                }
            ]
        })
    );
});