using Atv.CashControls.Dtos.Depts;
using Atv.CashControls.Services.Depts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Users;

namespace Atv.CashControls.Pages.Depts
{
    public class CreateModalModel : AbpPageModel
    {
        private readonly IDeptService _deptService;
        private readonly ICurrentUser _currentUser;

        public CreateModalModel(IDeptService deptService, ICurrentUser currentUser)
        {
            _deptService = deptService;
            _currentUser = currentUser;
        }

        [BindProperty]
        public CreateDeptViewModel DeptVM { get; set; }


        public async Task OnGetAsync()
        {
            DeptVM = new CreateDeptViewModel();

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var entity = ObjectMapper.Map<CreateDeptViewModel, CreateDeptsDto>(DeptVM);

            await _deptService.CreateAsync(entity);

            return NoContent();
        }
    }

    public class CreateDeptViewModel : CreateDeptsDto
    {

    }
}
