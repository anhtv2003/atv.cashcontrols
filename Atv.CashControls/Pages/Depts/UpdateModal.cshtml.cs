using Atv.CashControls.Dtos.Depts;
using Atv.CashControls.Entities;
using Atv.CashControls.Repositories.Depts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Users;

namespace Atv.CashControls.Pages.Depts
{
    public class UpdateModalModel : AbpPageModel
    {
        private readonly IDeptRepository _deptRepository;
        private readonly ICurrentUser _currentUser;

        public UpdateModalModel(IDeptRepository deptRepository, ICurrentUser currentUser)
        {
            _deptRepository = deptRepository;
            _currentUser = currentUser;
        }

        [BindProperty]
        [HiddenInput]
        public Guid Id { get; set; }

        [BindProperty]
        [HiddenInput]
        public Guid UserId { get; set; }

        [BindProperty]
        public UpdateDeptViewModel DeptVM { get; set; }

        public async Task OnGetAsync()
        {
            var dept = await _deptRepository.GetAsync(Id);           

            DeptVM = ObjectMapper.Map<Dept, UpdateDeptViewModel>(dept);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            return NoContent();
        }
    }

    public class UpdateDeptViewModel : UpdateDeptsDto { }
}
