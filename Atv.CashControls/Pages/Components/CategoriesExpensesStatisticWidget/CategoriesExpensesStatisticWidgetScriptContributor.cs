﻿using Volo.Abp.AspNetCore.Mvc.UI.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Packages.ChartJs;
using Volo.Abp.Modularity;

namespace Atv.CashControls.Pages.Components.CategoriesExpensesStatisticWidget
{
    [DependsOn(typeof(ChartjsScriptContributor))]
    public class CategoriesExpensesStatisticWidgetScriptContributor : BundleContributor
    {
        public override void ConfigureBundle(BundleConfigurationContext context)
        {
            context.Files.Add("/Pages/Components/CategoriesExpensesStatisticWidget/Default.js");
        }
    }
}
