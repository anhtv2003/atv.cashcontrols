﻿$(function () {
    var categoriesExpensesStatisticWidget = abp.widgets.CategoriesExpensesStatisticWidget = function ($wrapper) {
        var _chart;
        var _latestFilter;

        var getFilters = () => {
            return {
                month: $('#category-expense-month-filter').val(),
                year: $('#category-expense-year-filter').val(),
            }
        }

        var refreshChart = (statistic) => {
            console.log(statistic);

            _chart.data = {
                labels: Object.keys(statistic.data),
                datasets: [
                    {
                        label: "Category",
                        data: Object.values(statistic.data),
                        backgroundColor: [
                            'rgba(50, 150, 255, 1)',
                            'rgba(150, 255, 100, 1)',
                            'rgba(255, 100, 150, 1)',
                            'rgba(203, 243, 210, 1)',
                            'rgba(112, 103, 207, 1)',
                            'rgba(123, 40, 125, 1)',
                            'rgba(216, 99, 203, 1)',
                            'rgba(144, 46, 59, 1)',
                            'rgba(144, 78, 202, 1)',
                        ]
                    }
                ]
            };

            _chart.update();
        }

        var render = (filter, callback) => {
            _latestFilter = {
                month: filter.month,
                year: filter.year,
            }

            abp.ajax({
                url: '/api/cash-controls/dashboard/category-expense-statistic-widget',
                data: _latestFilter,
                type: 'GET'
            })
                .done((res) => {
                    callback(res);
                })
        }

        var refresh = (filters) => {
            render(filters, refreshChart);
        }

        var init = (filter) => {
            console.log(filter)

            _chart = new Chart($wrapper.find('.categoryExpensesStatisticChart'),
                {
                    type: 'pie',
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: ''
                            }
                        }
                    },
                });

            refresh(filter);

            console.log(filter)

        }

        return {
            getFilters: getFilters,
            init: init,
            refresh: refresh
        }
    }

    var instance = categoriesExpensesStatisticWidget($('#CategoriesExpensesStatisticWidgetWrap'));
    instance.init(instance.getFilters());
})