﻿using Atv.CashControls.Pages.Components.DayExpensesStatisticWidget;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Widgets;

namespace Atv.CashControls.Pages.Components.CategoriesExpensesStatisticWidget
{
    [Widget(
        ScriptTypes = new[] { typeof(CategoriesExpensesStatisticWidgetScriptContributor) })]
    public class CategoriesExpensesStatisticWidgetViewComponent : AbpViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
