﻿$(function () {
    var dayExpensesStatisticWidget = abp.widgets.DayExpensesStatisticWidget = function ($wrapper) {
        let _chart;
        let _latestFilter;

        function formatDate(date) {
            return luxon.DateTime.fromISO(date, {
                locale: abp.localization.currentCulture.name
            }).toFormat('MM/dd/yyyy');
        }

        var getFilters = function () {
            return {
                month: $('#day_expenses_month_filter').val(),
                year: $('#day_expenses_year_filter').val()
            };
        }

        var refreshChart = (filter) => {
            _latestFilter = {
                month: filter.month,
                year: filter.year,
            };

            abp.ajax({
                url: `/api/cash-controls/dashboard/get-day-expenses-statistic-widget`,
                type: 'GET',
                data: _latestFilter
            })
                .then((res) => {
                    _chart.data = {
                        labels: Object.keys(res.data),
                        datasets: [
                            {
                                label: 'Expense statistic',
                                data: Object.values(res.data),
                                backgroundColor: 'rgba(255, 132, 132, 1)'
                            }
                        ]
                    };
                    _chart.update();
                }).catch(err => {
                    console.log(err);
                });
        }

        var init = (filter) => {
            let delayed;

            _chart = new Chart($wrapper.find('.DayExpensesStatisticChart'),
                {
                    type: 'bar',
                    options: {
                        responsive: true,
                        scales: {
                            yAxes: [
                                {
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }
                            ]
                        },
                        plugins: {                            
                            title: {
                                display: true,
                                text: 'Month Expenses statistic'
                            }
                        },
                        animation: {
                            onComplete: () => {
                                delayed = true;
                            },
                            delay: (context) => {
                                let delay = 0;
                                if (context.type === 'data' && context.mode === 'default' && !delayed) {
                                    delay = context.dataIndex * 300 + context.datasetIndex * 100;
                                }
                                return delay;
                            },
                        }
                    }
                });

            refreshChart(filter);

            $wrapper
                .find('#day_expenses_month_filter')
                .on('change',
                    function () {
                        refreshChart($.extend(_latestFilter, getFilters()));
                    });

            $wrapper
                .find('#day_expenses_year_filter')
                .on('change', function () {
                    refreshChart($.extend(_latestFilter, getFilters()));
                });
        }

        return {
            getFilters: getFilters,
            init: init,
            refresh: refreshChart
        }
    };

    var instance = dayExpensesStatisticWidget($('#dayExpensesStatisticWidgetWrapper'));
    instance.init(instance.getFilters());

})