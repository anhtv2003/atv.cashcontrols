﻿using Atv.CashControls.Pages.Components.DayExpensesStatisticWidget;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Widgets;

namespace Atv.CashControls.Pages.Components.DayExpensesWidget
{
    [Widget(
        ScriptTypes = new[] { typeof(DayExpensesStatisticWidgetScriptContributor) })]
    public class DayExpensesStatisticWidgetViewComponent : AbpViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
