using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Dtos.Wallets;
using Atv.CashControls.Services.BankingNotes;
using Atv.CashControls.Services.Wallets;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Wallets
{
    public class CreateModalModel : AbpPageModel
    {
        private readonly IWalletService _walletService;
        private readonly IBankingNoteService _bankingNoteService;

        public CreateModalModel(IWalletService walletService, IBankingNoteService bankingNoteService)
        {
            _walletService = walletService;
            _bankingNoteService = bankingNoteService;
        }

        [BindProperty]
        public CreateWalletViewModel WalletVM { get; set; }

        [BindProperty]
        public IEnumerable<BankingNoteDto> BankingNoteVM { get; set; }


        public async Task OnGetAsync()
        {
            WalletVM = new CreateWalletViewModel();
            BankingNoteVM = (await _bankingNoteService.GetListAync(new GetBankingNoteInput { FilterText = null })).Items;

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var dto = ObjectMapper.Map<CreateWalletViewModel, CreateWalletDto>(WalletVM);
            await _walletService.CreateAsync(dto);

            return NoContent();
        }
    }

    public class CreateWalletViewModel : CreateWalletDto { }
}
