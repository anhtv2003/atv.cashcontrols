using Atv.CashControls.Dtos.BankingNotes;
using Atv.CashControls.Dtos.Wallets;
using Atv.CashControls.Entities;
using Atv.CashControls.Services.BankingNotes;
using Atv.CashControls.Services.Wallets;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Wallets
{
    public class UpdateModalModel : AbpPageModel
    {
        private readonly IWalletService _walletService;
        private readonly IBankingNoteService _bankingNoteService;

        public UpdateModalModel(IWalletService walletService, IBankingNoteService bankingNoteService)
        {
            _walletService = walletService;
            _bankingNoteService = bankingNoteService;
        }

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid Id { get; set; }

        [BindProperty]
        public UpdateWalletViewModel WalletVM { get; set; }

        [BindProperty]
        public IEnumerable<BankingNoteDto> BankingNoteVM { get; set; }

        public async Task OnGetAsync()
        {
            WalletVM = ObjectMapper.Map<WalletDto, UpdateWalletViewModel>(await _walletService.GetAsync(Id));
            BankingNoteVM = (await _bankingNoteService.GetListAync(new GetBankingNoteInput { FilterText = null })).Items;

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var dto = ObjectMapper.Map<UpdateWalletViewModel, UpdateWalletDto>(WalletVM);

            await _walletService.UpdateAsync(Id, dto);

            return NoContent();
        }
    }

    public class UpdateWalletViewModel : UpdateWalletDto { }
}
