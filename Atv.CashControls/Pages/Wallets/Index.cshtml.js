﻿$(function () {
    var l = abp.localization.getResource('CashControls');

    const createModal = new abp.ModalManager(abp.appPath + 'Wallets/CreateModal');
    const updateModal = new abp.ModalManager(abp.appPath + 'Wallets/UpdateModal');

    $(document).on('input', '#WalletVM_Money', function () {
        var value = $(this).val().replace(/\D/g, '');

        if (value) {
            value = new Intl.NumberFormat('de-DE').format(value);
            $(this).val(value);
        }
    })

    // Loại bỏ dấu chấm trước khi submit form
    $(document).on('submit', '#frmCreateWallet', function () {
        var priceInput = $('#WalletVM_Money');
        var rawValue = priceInput.val().replace(/\./g, ''); // Loại bỏ dấu phân cách
        priceInput.val(rawValue); // Gán giá trị không có dấu phân cách lại cho input
    });

    $('#createBtn').on('click', function (e) {
        e.preventDefault();

        createModal.open();
    });

    createModal.onResult(function () {
        abp.notify.success(l('SuccessfullyCreated'));
        datatable.ajax.reload();
    });

    updateModal.onResult(function () {
        abp.notify.success(l("SuccessfullyUpdated"));
        datatable.ajax.reload();
    })

    $(document).on('change', '#WalletVM_BankingConnetion', function () {
        if ($(this).is(':checked')) {
            //$('#WalletVM_Money').prop('disabled', true);
            $('#connectBanking').removeClass('d-none');
            $('#connectBanking').addClass('d-block');
        } else {
            //$('#WalletVM_Money').prop('disabled', false);
            $('#connectBanking').removeClass('d-block');
            $('#connectBanking').addClass('d-none');
        }
    });

    $(document).on('change', '#WalletVM_BankingId', function () {
        const bankingId = $(this).val();

        if (bankingId) {
            abp.ajax({
                url: '/api/cash-controls/banking-notes/get-money/' + bankingId,
                method: 'GET'
            }).done((res) => {
                $('#WalletVM_Money').val(res);
                //$('#WalletVM_Name').val(`Tai khoản ` + )
            });
        }
    })


    var datatable = $('#walletsDataTable').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            processing: true,
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            scrollCollapse: true,
            ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.wallet.getList),
            columnDefs: [
                {
                    title: l("Actions"),
                    visible: function (data) {
                        if (data.bankingMapping === 1) {
                            return false;
                        }
                        else
                            return true;
                    },
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                iconClass: "fas fa-pen-alt text-primary",
                                visible: function (data) {
                                    return data.bankingMapping === 0 && abp.auth.isGranted('CashControls.Wallets.Delete');;
                                },
                                action: function (data) {
                                    updateModal.open({ id: data.record.id });
                                }
                            },
                            {
                                text: l("Delete"),
                                iconClass: "fas fa-trash-alt text-danger",
                                visible: function (data) {
                                    if (data.bankingMapping === 1) {
                                        return false;
                                    }
                                    else {
                                        return abp.auth.isGranted('CashControls.Wallets.Delete');
                                    }

                                },
                                confirmMessage: function () {
                                    return l("DeleteConfirmMessage");
                                },
                                action: function (data) {
                                    atv.cashControls.controllers.wallet
                                        .delete(data.record.id)
                                        .then(function () {
                                            abp.notify.success(l('SuccessfullyDeleted'));
                                            datatable.ajax.reload();
                                        })
                                }
                            }
                        ]
                    }
                },
                {
                    title: l('Name'),
                    data: 'name'
                },
                {
                    title: l('Price'),
                    data: "money",
                    render: function (data) {
                        var currentName = abp.localization.currentCulture.name;

                        return new Intl.NumberFormat(currentName, {
                            style: 'currency',
                            currency: currentName === 'vi' ? 'VND' : 'USD'
                        }).format(data);
                    }
                },

            ]
        })
    );
});