using Atv.CashControls.Dtos.Expenses;
using Atv.CashControls.Dtos.Incomes;
using Atv.CashControls.Pages.Expenses;
using Atv.CashControls.Services.Incomes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Incomes
{
    public class CreateModalModel : AbpPageModel
    {
        private readonly IIncomeService _service;

        public CreateModalModel(IIncomeService service)
        {
            _service = service;
        }

        [BindProperty]
        public IncomesViewModal IncomeVM { get; set; }

        [BindProperty]
        [HiddenInput]
        public Guid UserId { get; set; }

        public async Task OnGetAsync()
        {
            IncomeVM = new IncomesViewModal();

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var entity = ObjectMapper.Map<IncomesViewModal, CreateIncomeDto>(IncomeVM);

            await _service.CreateAsync(entity);

            return NoContent();
        }
    }

    public class IncomesViewModal : CreateIncomeDto { }
}
