using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Dtos.Incomes;
using Atv.CashControls.Entities;
using Atv.CashControls.Pages.CashCategories;
using Atv.CashControls.Services.Incomes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Incomes
{
    public class EditModalModel : AbpPageModel
    {
        private readonly IIncomeService _service;

        public EditModalModel(IIncomeService service)
        {
            _service = service;
        }

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid Id { get; set; }

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid UserId { get; set; }

        [BindProperty]
        public UpdateIncomeViewModel IncomeVM { get; set; }

        public async Task OnGet()
        {
            var dto = await _service.GetAsync(Id);
            IncomeVM = ObjectMapper.Map<IncomeDto, UpdateIncomeViewModel>(dto);
        }

        public async Task<NoContentResult> OnPostAsync()
        {
            var model = ObjectMapper.Map<UpdateIncomeViewModel, UpdateIncomeDto>(IncomeVM);
            await _service.UpdateAsync(Id, model);
            return NoContent();
        }
    }

    public class UpdateIncomeViewModel : UpdateIncomeDto { }
}
