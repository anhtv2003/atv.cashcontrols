﻿$(function () {
    var l = abp.localization.getResource("CashControls");

    var createModal = new abp.ModalManager(abp.appPath + "Incomes/CreateModal");
    var updateModal = new abp.ModalManager(abp.appPath + "Incomes/EditModal");

    $("#btn_CreateIncome").on("click", function (e) {
        e.preventDefault();
        createModal.open();
    });

    createModal.onResult(function () {
        abp.notify.success(l("SuccessfullyCreated"));
        dataTable.ajax.reload();
    });

    updateModal.onResult(function () {
        abp.notify.success(l("SuccessfullyUpdated"));
        dataTable.ajax.reload();
    });

    var getFilter = function () {
        return {
            filterText: $("#FilterText").val(),
        };
    };

    var dataTable = $("#incomesDatatable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            processing: true,
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            scrollCollapse: true,
            ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.income.getListAync, getFilter),
            columnDefs: [
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                iconClass: "fas fa-pen-alt",
                                visible: abp.auth.isGranted("CashControls.Incomes.Edit"),
                                action: function (data) {
                                    console.log(data.record.id);
                                    updateModal.open({ id: data.record.id });
                                },
                            },
                            {
                                text: l("Delete"),
                                iconClass: "fas fa-trash-alt",
                                visible: abp.auth.isGranted("CashControls.Incomes.Delete"),
                                confirmMessage: function () {
                                    return l("DeleteConfirmMessage");
                                },
                                action: function (data) {
                                    atv.cashControls.controllers.income.delete(data.record.id).then(function () {
                                        abp.notify.success(l("SuccessfullyDeleted"));
                                        dataTable.ajax.reload();
                                    });
                                },
                            },
                        ],
                    },
                },
                {
                    title: l("Date"),
                    data: "date",
                    render: function (data) {
                        return luxon.DateTime.fromISO(data, {
                            locale: abp.localization.currentCulture.name,
                        }).toLocaleString(luxon.DateTime.DATETIME_SHORT);
                    },
                },      
                {
                    title: l("Amount"),
                    orderable: true,
                    data: "amount",
                    render: function (data) {
                        var currentName = abp.localization.currentCulture.name;

                        return new Intl.NumberFormat(currentName, {
                            style: "currency",
                            currency: currentName === "vi" ? "VND" : "USD",
                        }).format(data);
                    },
                },
                {
                    title: l("Type"),
                    data: "type",
                },
                {
                    title: l("Source"),
                    data: "source",
                },
                {
                    title: l("Description"),
                    data: "description",
                },
                
            ],
        })
    );
});