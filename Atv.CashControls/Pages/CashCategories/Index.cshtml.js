﻿$(function () {
    var l = abp.localization.getResource("CashControls");

    var createModal = new abp.ModalManager(abp.appPath + "CashCategories/CreateModal");
    var updateModal = new abp.ModalManager(abp.appPath + "CashCategories/EditModal");

    createModal.onResult(function () {
        abp.notify.success(l("SuccessfullyCreated"));
        dataTable.ajax.reload();
    });

    $("#createModalBtn").on("click", function (e) {
        e.preventDefault();
        createModal.open();
    });

    updateModal.onResult(function () {
        abp.notify.success(l("SuccessfullyUpdated"));
        dataTable.ajax.reload();
    });

    var getFilter = function () {
        return {
            filterText: $("#FilterText").val(),
            name: $("#NameFilter").val(),
        };
    };

    $("#searchForm").submit(function (e) {
        e.preventDefault();
        dataTable.ajax.reload();
    });

    $("#exportDataBtn").on("click", function (e) {
        e.preventDefault();
    });

    var dataTable = $("#CashCategoriesTable").DataTable(
        abp.libs.datatables.normalizeConfiguration({
            processing: true,
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            scrollCollapse: true,
            ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.cashcategory.getListAync, getFilter),
            columnDefs: [
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                iconClass: "fas fa-pen-alt",
                                visible: abp.auth.isGranted("CashControls.CashCategories.Edit"),
                                action: function (data) {
                                    console.log(data.record.id);
                                    updateModal.open({ id: data.record.id });
                                },
                            },
                            {
                                text: l("Delete"),
                                iconClass: "fas fa-trash-alt",
                                visible: abp.auth.isGranted("CashControls.CashCategories.Delete"),
                                confirmMessage: function () {
                                    return l("DeleteConfirmMessage");
                                },
                                action: function (data) {
                                    atv.cashControls.controllers.cashcategory.delete(data.record.id).then(function () {
                                        abp.notify.success(l("SuccessfullyDeleted"));
                                        dataTable.ajax.reload();
                                    });
                                },
                            },
                        ],
                    },
                },
                {
                    title: l("Name"),
                    data: "name",
                },
                {
                    title: l("Description"),
                    data: "description",
                },
                {
                    title: l("Creation_Time"),
                    data: "creationTime",
                    render: function (data) {
                        return luxon.DateTime.fromISO(data, {
                            locale: abp.localization.currentCulture.name,
                        }).toLocaleString(luxon.DateTime.DATETIME_SHORT);
                    },
                },
                {
                    title: l("User_Name"),
                    data: "creatorId",
                    render: function (data, type, row, meta) {
                        // Tạo một placeholder cho đến khi có dữ liệu từ API
                        var userNamePlaceholder = '<span id="user-name-' + meta.row + '">Loading...</span>';

                        abp.ajax({
                            url: `/api/identity/users/${data}`,
                            type: 'GET'
                        })
                            .then(function (res) {
                                $('#user-name-' + meta.row).text(res.userName);
                            })
                            .catch(function (err) {
                                console.error(err);
                                $('#user-name-' + meta.row).text('Error loading');
                            });

                        return userNamePlaceholder;
                    }
                },
                {
                    title: l("Status"),
                    data: "status",
                    render: function (data) {
                        return `
                        <span class="badge bg-success">${l("Enum:Status." + data) }</span>
                        `;
                    },
                },
            ],
        })
    );

    $("#AdvancedFilterSectionToggler").on("click", function () {
        $("#AdvancedFilterSection").slideToggle();
    });

    $(".comming-soon").on("click", function () {
        abp.notify.warn("Comming soon!");
    });
});
