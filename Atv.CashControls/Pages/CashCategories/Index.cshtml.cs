using Atv.CashControls.Services.CashCategories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.CashCategories
{
    public class IndexModel : AbpPageModel
    {
        private readonly ICashCategoryService _service;

        public string? NameFilter { get; set; }
        public string? DescriptionFilter { get; set; }
        public DateTime? CreationDateFilterMin { get; set; }
        public DateTime? CreationDateFilterMax { get; set; }

        public IndexModel(ICashCategoryService service)
        {
            _service = service;
        }

        public async Task OnGetAsync()
        {
            await Task.CompletedTask;
        }
    }

}
