using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Services.CashCategories;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.CashCategories
{
    public class EditModalModel : AbpPageModel
    {
        private readonly ICashCategoryService _service;

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid Id { get; set; }

        [HiddenInput]
        [BindProperty(SupportsGet = true)]
        public Guid UserId { get; set; }

        [BindProperty]
        public UpdateCashCategoryViewModel CashCategoryVM { get; set; }

        public EditModalModel(ICashCategoryService service)
        {
            _service = service;
        }

        public async Task OnGetAsync()
        {
            var cashCategory = await _service.GetAsync(Id);
            CashCategoryVM = ObjectMapper.Map<CashCategoryDto, UpdateCashCategoryViewModel>(cashCategory);
        }

        public async Task<NoContentResult> OnPostAsync ()
        {
            var cashCategory = ObjectMapper.Map<UpdateCashCategoryViewModel, UpdateCashCategoryDto>(CashCategoryVM);
            await _service.UpdateAsync(Id, cashCategory);
            return NoContent();
        }
    }

    public class UpdateCashCategoryViewModel : UpdateCashCategoryDto
    {

    }
}
