using Atv.CashControls.Dtos.CashCategories;
using Atv.CashControls.Services.CashCategories;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;
using Volo.Abp.Users;

namespace Atv.CashControls.Pages.CashCategories
{
    public class CreateModalModel : AbpPageModel
    {
        private readonly ICashCategoryService _service;
        private readonly ICurrentUser _currentUser;

        [BindProperty]
        public CreateCashCategoryViewModel CashCategoryVM { get; set; }

        public CreateModalModel(ICashCategoryService service, ICurrentUser currentUser)
        {
            _service = service;
            _currentUser = currentUser;
        }

        public async Task OnGetAsync()
        {
            CashCategoryVM = new CreateCashCategoryViewModel();
            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var entity = ObjectMapper.Map<CreateCashCategoryViewModel, CreateCashCategoryDto>(CashCategoryVM);

            await _service.CreateAsync(entity);
            return NoContent();
        }
    }

    public class CreateCashCategoryViewModel : CreateCashCategoryDto { }

}
