﻿using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc;

namespace Atv.CashControls.Pages.Shared.Components.Notifications
{
    public class NotificationViewComponent : AbpViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View("/Pages/Shared/Components/Notifications/Default.cshtml");
        }
    }
}
