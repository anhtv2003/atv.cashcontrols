using Atv.CashControls.Dtos.Loans;
using Atv.CashControls.Services.Loans;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace Atv.CashControls.Pages.Loans
{
    public class CreateModalModel : AbpPageModel
    {
        private readonly ILoanService _loanService;

        public CreateModalModel(ILoanService loanService)
        {
            _loanService = loanService;
        }

        [BindProperty]
        public CreateLoanViewModel LoanVM { get; set; }


        public async Task OnGetAsync()
        {
            LoanVM = new CreateLoanViewModel();

            await Task.CompletedTask;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var entity = ObjectMapper.Map<CreateLoanViewModel, CreateLoansDto>(LoanVM);

            await _loanService.CreateAsync(entity);

            return NoContent();
        }
    }

    public class CreateLoanViewModel : CreateLoansDto
    {

    }
}
