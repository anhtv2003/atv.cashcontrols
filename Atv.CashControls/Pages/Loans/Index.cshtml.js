﻿$(function () {
    var l = abp.localization.getResource('CashControls');

    const createModal = new abp.ModalManager(abp.appPath + 'Loans/CreateModal');

    $('#btn_CreateLoan').on('click', function (e) {
        e.preventDefault();
        createModal.open();
    });

    createModal.onResult(function () {
        abp.notify.success(l('SuccessfullyCreated'));
        datatable.ajax.reload();
    })

    $(document).on('input', '#LoanVM_Amount', function () {
        var value = $(this).val().replace(/\D/g, '');

        console.log(value)

        if (value) {
            value = new Intl.NumberFormat('de-DE').format(value);
            $(this).val(value);
        }
    })

    // Loại bỏ dấu chấm trước khi submit form
    $(document).on('submit', '#frmCreateLoan', function () {
        var priceInput = $('#LoanVM_Amount');
        var rawValue = priceInput.val().replace(/\./g, ''); // Loại bỏ dấu phân cách
        priceInput.val(rawValue); // Gán giá trị không có dấu phân cách lại cho input
    });

    var datatable = $('#loansDatatable').DataTable(
        abp.libs.datatables.normalizeConfiguration({
            processing: true,
            serverSide: true,
            paging: true,
            order: [[1, "asc"]],
            searching: false,
            scrollX: true,
            scrollCollapse: true,
            ajax: abp.libs.datatables.createAjax(atv.cashControls.controllers.loans.getList),
            columnDefs: [
                {
                    title: l("Actions"),
                    rowAction: {
                        items: [
                            {
                                text: l("Edit"),
                                iconClass: "fas fa-pen-alt text-primary",
                                visible: abp.auth.isGranted('CashControls.Loans.Edit'),
                                action: function (data) {
                                    //editModal.open({ id: data.record.id });
                                }
                            },
                            {
                                text: l("Delete"),
                                iconClass: "fas fa-trash-alt text-danger",
                                visible: abp.auth.isGranted('CashControls.Loans.Delete'),
                                confirmMessage: function () {
                                    return l("DeleteConfirmMessage");
                                },
                                action: function (data) {
                                    atv.cashControls.controllers.loans
                                        .delete(data.record.id)
                                        .then(function () {
                                            abp.notify.success(l('SuccessfullyDeleted'));
                                            datatable.ajax.reload();
                                        }).catch(function (e) {
                                            console.log("Error detail: ");
                                            console.log(e);
                                            abp.notify.error(`Error: ${e.message} (Code: ${e.code})`);
                                        })
                                }
                            }

                        ]
                    }
                },
                {
                    title: l('Amount'),
                    data: "amount",
                    render: function (data) {
                        var currentName = abp.localization.currentCulture.name;

                        return new Intl.NumberFormat(currentName, {
                            style: 'currency',
                            currency: currentName === 'vi' ? 'VND' : 'USD'
                        }).format(data);
                    }
                },
                {
                    title: l('Borrower'),
                    data: 'borrower'
                },
                {
                    title: l('Date'),
                    data: "date",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATE_SHORT);
                    }
                },
                {
                    title: l('DueDate'),
                    data: "dueDate",
                    render: function (data) {
                        return luxon
                            .DateTime
                            .fromISO(data, {
                                locale: abp.localization.currentCulture.name
                            }).toLocaleString(luxon.DateTime.DATE_SHORT);
                    }
                },
                {
                    title: l('Status'),
                    data: 'status',
                    render: function (data) {
                        return data === 0 ?
                            `<span class="badge text-bg-danger">${l('Enum:DeptStatusEnum.' + data)}</span>` :
                            `<span class="badge text-bg-success text-white">${l('Enum:DeptStatusEnum.' + data)}</span>`;
                    }
                }
            ]
        })
    );
})