﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.DependencyInjection;

namespace Atv.CashControls.Data;

public class CashControlsEFCoreDbSchemaMigrator : ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public CashControlsEFCoreDbSchemaMigrator(
        IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task MigrateAsync()
    {
        /* We intentionally resolve the CashControlsDbContext
         * from IServiceProvider (instead of directly injecting it)
         * to properly get the connection string of the current tenant in the
         * current scope.
         */

        await _serviceProvider
            .GetRequiredService<CashControlsDbContext>()
            .Database
            .MigrateAsync();
    }
}
