﻿using Atv.CashControls.Entities;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.AuditLogging.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.FeatureManagement.EntityFrameworkCore;
using Volo.Abp.Identity.EntityFrameworkCore;
using Volo.Abp.OpenIddict.EntityFrameworkCore;
using Volo.Abp.PermissionManagement.EntityFrameworkCore;
using Volo.Abp.SettingManagement.EntityFrameworkCore;
using Volo.Abp.TenantManagement.EntityFrameworkCore;

namespace Atv.CashControls.Data;

public class CashControlsDbContext : AbpDbContext<CashControlsDbContext>
{
    public CashControlsDbContext(DbContextOptions<CashControlsDbContext> options)
        : base(options)
    {
    }

    /***
     * 
     * Username: admin
     * Password: 1q2w3E*
     * 
     **/

    public DbSet<CashCategory> CashCategories { get; set; }
    public DbSet<Expense> Expenses { get; set; }
    public DbSet<Dept> Depts { get; set; }
    public DbSet<Loan> Loans { get; set; }
    public DbSet<BankingNote> BankingNotes { get; set; }
    public DbSet<BankingNoteTransaction> BankingNoteTransactions { get; set; }
    public DbSet<Wallet> Wallets { get; set; }
    public DbSet<Income> Incomes { get; set; }


    // Demo
    public DbSet<Book> Books { get; set; }

    public const string DbTablePrefix = "";
    public const string DbSchema = null;

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        /* Include modules to your migration db context */

        builder.ConfigurePermissionManagement();
        builder.ConfigureSettingManagement();
        builder.ConfigureAuditLogging();
        builder.ConfigureIdentity();
        builder.ConfigureOpenIddict();
        builder.ConfigureFeatureManagement();
        builder.ConfigureTenantManagement();

        /* Configure your own entities here */
        if (builder.IsHostDatabase())
        {
            builder.Entity<CashCategory>(x =>
            {
                x.ToTable(DbTablePrefix + nameof(CashCategories), DbSchema);
                x.ConfigureByConvention(); //auto configure for the base class props
                x.Property(x => x.Name).HasColumnName(nameof(CashCategory.Name)).IsRequired().HasMaxLength(128);
                x.Property(x => x.Description).HasColumnName(nameof(CashCategory.Description)).IsRequired(false);
            });

            builder.Entity<Expense>(x =>
            {
                x.ToTable(DbTablePrefix + nameof(Expenses), DbSchema);
                x.ConfigureByConvention();
                x.Property(x => x.Name).HasColumnName(nameof(Expense.Name)).IsRequired().HasMaxLength(128);
                x.Property(x => x.Price).HasColumnName(nameof(Expense.Price)).IsRequired();
                x.Property(x => x.Quantity).HasColumnName(nameof(Expense.Quantity)).HasDefaultValueSql("1");
                x.Property(x => x.Note).HasColumnName(nameof(Expense.Note)).IsRequired(false);
                x.Property(x => x.CategoryID).HasColumnName(nameof(Expense.CategoryID)).IsRequired(false);
                x.Property(x => x.Date).HasColumnName(nameof(Expense.Date)).IsRequired().HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            builder.Entity<Dept>(x =>
            {
                x.Property(e => e.Lender).HasComment("Người cho vay.");
                x.Property(e => e.DueDate).HasComment("Ngày đến hạn.").HasDefaultValueSql("CURRENT_TIMESTAMP");
                x.Property(e => e.ActualPaymentDate).HasComment("Ngày trả thực tế.").HasDefaultValueSql("CURRENT_TIMESTAMP");
                x.Property(e => e.Date).HasComment("Ngày vay.").HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            builder.Entity<Loan>(x =>
            {
                x.Property(e => e.Borrower).HasComment("Người vay.");
                x.Property(e => e.DueDate).HasComment("Ngày người vay thanh toán.").HasDefaultValueSql("CURRENT_TIMESTAMP");
                x.Property(e => e.Date).HasComment("Ngày cho vay.").HasDefaultValueSql("CURRENT_TIMESTAMP");
            });
        }
    }
}
